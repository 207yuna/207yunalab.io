---
title: "About"
description: "Studentessa laureanda in Scienze delle produzioni animali, classe L-38"
norss: true
nosearch: true
showDate: false
---

{{< figure src="/photo_about.jpg" width="300" class="rightfloat" >}}

Appassionata del settore zootecnico, veterinario e biotecnologico.

Non c'è un momento nella mia vita in cui non ricordi di non aver avuto un animale di fianco: dai miei primi passi sino al tempo presente, sono sempre stata accompagnata nel mio viaggio da forme di vita diverse: dai pesci, alle papere, alle tartarughe, ai gatti e ai cani (un'enorme fortuna e benedizione da un lato, grandi compromessi da altri, primo tra tutti l'irrefrenabile desiderio di vivere nuove esperienze con animali diversi come rettili, insetti, bovini o piccole caprette, desiderio impossibile per una bambina risiedente in un appartamento di città).

Col passare del tempo, ho realizzato che il mio non fosse un semplice capriccio mascherato dal desiderio di novità, ma una forte necessità di conoscere qualsiasi creatura intorno a me e studiarla, analizzarla, interagirvi il più possibile. Questo è uno dei tanti motivi per il quale la mia passione per il mondo animale non conosce limiti (o quantomento, andrebbero di certo al di fuori dei 2600 caratteri di informazioni personali e oltre la soglia di curiosità dei lettori del mio profilo).

Al di fuori delle mie esperienze accademiche e lavorative, continuo a coltivare come hobby le curiosità sul mondo animale da documentari, libri (che da soli riuscirebbero a riempire le librerie più esigenti), attività di volontariato ai canili e larga esperienza di dog/cat sitting (e direi anche, se esistesse ufficialmente il termine, occasionalmente rabbit-sitting).

Se avessi più tempo a disposizione, probabilmente, insieme alla laurea in scienze delle produzioni animali, corteggerei la laurea in entomologia, biologia marina, veterinaria.
