---
title: "Pregnancy toxemia in small ruminants: ewes and does analysis and prophylaxis management"
date: 2022-11-11T15:10:28+01:00
tags:
  - pregnancy toxemia
  - goat
  - sheep
  - ketonemia
  - ketogenesis
  - ketosis
  - management
  - diagnosis
  - treatment
  - prevention
image: "/posts/img/pregnancy_toxemia/cover.jpg"
---

**Mentor:** Professor Stefano Comazzi

**Accademic Year:** 2021/2022

# Abstract

Pregnancy toxemia is reckoned to be one of the most frequent metabolic
disorders typically associated with animals, such as sheep and goats,
that typically carry bi- or plurigemellar pregnancies to term. The main
triggers are the presence of multiple fetuses (with higher energy
demands to meet than a single one), malnutrition or overfeeding, and
particularly stressful events such as adverse weather conditions, sudden
changes in feeding, concomitant diseases, or transportation. It is
essential to highlight that, during pregnancy, appetite gradually
decreases and the volume of the gravid uterus (compressing the rumen)
limits the amount of food that can be ingested; therefore, the loss of
an external source of energy predisposes the animal to use its own
energy reserves.

The condition occurs in the last 30 days of gestation and is most common
after the first pregnancy. In affected animals, the mortality rate is
particularly high.

As gestation progresses, fetuses' demand for blood glucose increases to
stimulate their development; the main energy resource for the fetus is
glucose, which is taken at the expense of the mother (as the body
diverts energy priority directly to the fetuses). Toxemia is at risk of
onset when blood glucose levels fall below a threshold limit of 30
mg/dl, causing hypoglycemia.

When glucose resources fall progressively and the liver cannot cope with
the shortage, energy requirements are taken from other metabolic
pathways such as amino acids and free fatty acids.

With lipomobilization of adipose reserves, concentrations of circulating
fatty acids increase in the bloodstream, which, when diverted to the
liver, if in amounts above the hepatic tolerance index (as it cannot
process them all), cause accumulation and clutter (predisposing the body
to steatosis) and are converted to ketone bodies (such as acetoacetate
and beta-hydroxybutyrate).

As ketogenesis (thus the formation of ketone bodies) progresses, the
level of ketones in the blood increases leading to hyperketonemia, and
at the same time the level of bicarbonate decreases causing acidosis
phenomena. As part of a diagnostic framework, the main clinical
manifestations to be detected are caused by a strong involvement of the
central nervous system.

The animal therefore exhibits signs of departure from the herd, often
accompanied by ambulatory distress, blindness, convulsions and lethargy,
reduced or absent appetite, until it falls into a comatose stage.

During the latter stages of the disease, urination decreases and kidney
function is impaired; blood glucose levels rise considerably (causing
hyperglycemia) in response to the stress discharged by the adrenal
glands.

On anatomopathologic examinations, the liver and occasionally the
kidneys are enlarged, steatotic, ranging in color from pale yellowish to
orange, and so overloaded with fat that they float in formalin. The
adrenal glands are often bulky, and the uterus may contain one or more
fetuses.

Differential diagnosis proves to be of particular importance in
discriminating toxemia from other pathological forms that often show
similarities in nervous manifestations: among these, special attention
should be paid to hypomagnesemia, hypocalcemia, and polyencephalomachia.

In contrast, diagnosis is mainly based on the anamnesis, clinical signs,
and the detection of high concentrations of ketone bodies in blood and
urine.

The therapeutic treatment to be tailored is closely related to how early
the disorder is identified in the course of the disease; if the animal
then exhibits behavioral abnormalities in the early stages of toxemia,
treatment involves correction aimed at rebalancing the negative energy
balance by stimulating appetite, raising blood glucose levels, and
restoring hydration with propylene glycol and glycerol solutions; in
late stages the animal has a poor prognosis and requires aggressive
treatment through hospitalization and use of fluid therapy, removal of
fetuses if necessary, followed by close monitoring until complete
recovery.

Because of its low survival rate following therapeutic treatment, the
best approach to manage cases of pregnancy toxemia in sheep and goats is
prevention.

This involves a unique identification system for each animal, accuracy
and precision on the recording of all cardinal events of pregnancy (such
as diagnosis, expected calving date) and a grouping, where possible, of
animals that share a common BCS and gestational stage.

Timely monitoring of the herd in terms of BCS and risk of concomitant
disease outbreaks is also essential, as well as an increased focus on
nutrition, an essential factor that marches hand in hand with increased
energy demands throughout the gestation period, through the feeding of
fresh, top-quality hay, accompanied if necessary by supplementary feeds
with particular regard to mineral and vitamin supplements.

# Introduction on ketosis on ruminants

Ketosis is a dismetabolic disease, a disorder that negatively alters the
body's processing and distribution of macronutrients such as proteins,
fats, and/or carbohydrates. Signately, ketosis is mainly characterized
by alterations of glucidic and lipidic metabolism.

It is an imbalance between the nutritive intake and the nutritive
requirements of the animal associated to impairment of glycemia. Its
main symptoms include an excessive amount of *ketone bodies* in the
cattle's body fluids.

Ketone bodies are a group of organic chemicals that are intermediary fat
metabolites and an alternative product of free fatty acid oxidation in
the liver that replace glucose as the main fuel of the brain in
situations of glucose scarcity. The formation process is termed
*ketogenesis*; they arise when the body derives energy from fat
molecules instead of drawing it from glucose ([Gulinski 2021](#ref-KetoneBodies)).

The lack of balance is indicated by several associated disturbances,
more or less marked such as ([Dye and Dougherty 1956](#ref-YearbookofAgriculture); [Steffeurd 1956](#ref-popo)):

- Hypoglicemia (low blood glucose levels);
- Depletion of liver glycogen (glucose stores);
- Mobilization of body proteins (as amino acids) to the liver for new production of glucose (gluconeogenesis);
- Mobilization of storage fat;
- Fatty infiltration of the liver;
- Increased production of ketone bodies;
- Increased ketone bodies in blood and urine, as the excessive production is eliminated throughout urine, milk and breath;
- Emaciation (loss of body weight);
- Lower milk production;
- Dehydration;
- More or less relevant steatosis.

This upset may be generally observed in all ruminants and high-producing
dairy cattle at the time of calving and in the onset of lactation, since
the metabolic requirements are increased about 100%
([Dye and Dougherty 1956](#ref-YearbookofAgriculture)).

During peripartum, in cattle with high genetic potential, the metabolic
priority is given first to the fetus, then to the udder. In other words:
the higher the genetic potential, the higher the priority will be given
to the development of the fetus and the production of milk (since sugar,
proteins and fats are requested to build milk, and the energy withdrawal
for production and secretion of the latter is also high)
([Fantini 2011](#ref-Ruminantia)).

Without any intervention to restore the energy balance and glycemia, the
increasing demand is more likely to result in the development of this
disorder.

Ketosis may be clinical or subclinical, and both forms are associated
with reduced milk production and reduced reproductive efficiency. In
addition, subclinical ketosis is more difficult to preemptively detect
and may become a risk factor for the development of clinical ketosis,
displaced abomasum, or both ([Edmondson and Pugh 2009](#ref-FoodAnimalPractice)).

Different types of recognized ketosis are classified in ruminants and
differentiated by the cause of ketone bodies stash
([Gulinski 2021](#ref-KetoneBodies); [Baird 1982](#ref-BovineKetosis)):

- Primary or Essential ketosis: most frequent between 1-8 weeks of lactation; near lactation peak, during periods of Negative Energy Balance, one of the reasons being poor quality food;
- Secondary ketosis: near the calving and up to the 1st week of postpartum; caused mainly by malnutrition in presence of anorectic factors. It can be accompanied by primary pathologies and/or occurs in the presence of elements that contribute to the animal's lack of appetite or cause digestive difficulties (such as dislocations, mastitis, metritis, fasciolosis);
- Nutritional ketosis: the main consequence of dietary errors, it may be caused excessive intake of butyric silages;
- Spontaneus ketosis: typically occurs in supsceptible high-yelding dairy cattle. The condition is not seen frequently in cows that are lactating for the first time;
- Gestational ketosis: due to increased glucose demand by foetus or mainly fetuses (in bigeminal pregnancies). It generally behave asymptomaticly up to the 7th month of pregnancy, then it becomes evident in latest 15-20 days prepartum;  Pregnancy Toxemia: similar to cattle gestational ketosis, frequently due to fetal needs (bigeminal or trigeminal pregnancies).

# Pregnancy toxaemia

*Pregnancy toxaemia* (PT), also known as *Twin kid disease*, *Ketosis*,
*Ketonemia* or *Hypoglycemia*, is a commonly known type of ketosis and
metabolic disorder patterned in ruminants, especially ewes and does,
associated with a faliure to adapt to the increasing metabolic and
glucose demand of fetal growth during late pregnancy. The name of the
disease originates from the similarity of the clinical symptoms that
resemble intoxication ([Farm Health Online 2018](#ref-FarmHealthOnlineToxemia); [Aikten 2007](#ref-DiseasesOfSheep)).

It represents an important area of survey since, unlike monotocic
ruminants, in polytocics it occurs in a more severe form due to the
presence of higher number of glucose-subtracting fetuses with greater
energetic demands from the mother aimed to satisfy proper growth.

It evolves around the final stage of gestation, both late pregnancy
(peripartuent toxaemia) and early lactation (postparturient toxaemia or
fatty liver syndrome), as the result of inappropriate metabolism of
*carbohydrates* and *fats*.

As observed in other ruminants beside ewes and goats, glucose
requirements are mostly met by gluconeogenesis, starting from Volatile
Fatty Acids (VFA) and other substrates, rather than by direct absorption
of carbohydrates ([Aikten 2007](#ref-DiseasesOfSheep)). Both conditions (glucose and fat
requirements) are closely linked and associated with *Negative Energy
Balance* (NEB) ([Farm Health Online 2018](#ref-FarmHealthOnlineToxemia)).

NEB occurs when the metabolizable energy from feed intake and body
reserves is less than required for the foetus to grow or the animal to
produce milk ([Farm Health Online 2018](#ref-FarmHealthOnlineToxemia)).

It is possible that the set of balance requirements is growing rapidly,
starting from a few weeks before birth to the peak of lactation; on the
other hand, the coverage of these needs is hindered by a rumen volume
that decreases with the growth of the uterus (especially in the presence
of multiple fetuses requiring adequate space), as well as
intra-abdominal fat deposits that reduce rumen capacity: this further
depresses appetite and voluntary food intake, leading to NEB
([Peri-Partum e chetogenesi n.d.](#ref-DocentiuniNa)).

Different etiology may lead to PTs, but they can be divided into five
broad categories:

1. Primary Pregnancy Toxemia: nutrient requirements are not met throughout food and feed: this is the most common manifestation;
2. Fat ewe/doe Pregnancy Toxemia: when ewes or does are overconditioned in early gestation. These animals may suffer a decline in nutrition during late gestation, which may be partially caused by smaller rumen capacity associated with the expanding uterus and large intra-abdominal fat deposits;
3. Starvation Pregnancy Toxemia: occurs in extremely thin sheep and goats usually because of lack of feed following periods of drought, heavy snow, or food;
4. Secondary Pregnancy Toxemia: usually concurs when other diseases are abroad, such as parasite infestations, impaired dentition, lameness and so on;
5. Stress-induced Pregnancy Toxemia: this is the least common cause of the disease, one where stress is the trigger. Examples are the close shepherding or housing of late-pregnant sheep of breeds not used to being housed, the transport of late pregnant sheep and outbreaks that occur following a period of flock attack by dogs ([Edmondson and Pugh 2009](#ref-FoodAnimalPractice); [Done et al. 2006](#ref-VeterinaryMedicine); [Radostis and al. 2006](#ref-ciao)).

Therefore, ketosis and fatty liver occur when physiologic mechanisms for
the adaptation to negative energy balance fail.

Failure of hepatic gluconeogenesis to supply adequate glucose for
lactation and body needs may be one cause of ketosis; however, poor
feedback control of nonesterified fatty acid (NEFA) release from adipose
tissue is another likely cause of ketosis and fatty liver.

The types of ketosis resulting from these two metabolic lesions may
require different therapeutic and prophylactic approaches
([Edmondson and Pugh 2009](#ref-FoodAnimalPractice); [Anderson and Rings 2009](#ref-pipi)).

# Pregnancy toxaemia in small ruminants

## Incidence and economic losses

In cows, prevalence rate of ketosis is closely related to diet and farm
management.

It has been found that it is higher in exotic pure/crossbreds than
native pure/non-descript. It is more prevalent during the years of
greatest milk production after the second, third, and later lactation
periods ([Dye and Dougherty 1956](#ref-YearbookofAgriculture)).

The incidence is found to be significantly associated with the cattle's
breed. There is a high prevalence of ketosis in animals of high genetic
potential, as they are unable to withstand the pressures arising from
the high nutritional demands generated by the production of milk, which
ultimately results in development of hypoglycemia that can persist as a
challenge for a successful dairy farming business. Researchers reported
that up to 33% tested positive for milk or urine ketones, weight loss,
and decreased milk production. About 50% in some high-producing herds
had at least subclinical ketosis and 20 to 30% of the subclinical cases
developed into clinical ketosis ([Madreseh-Ghahfarokhi, Azam Dehghani-Samani, and Amir Dehghani-Samani 2018](#ref-KetosisinDairyCattleFarms)).

In ewes, all sheep breeds and management systems are affected, but the
incidence is highest in lowground flocks of cross-bred ewes, typically
involving 1-2% of the ewes in well-managed flocks and up to 10% in
undernourished flocks.

The mortality rate is high and treatment is expensive and generally
unsuccessful, unless the disease is recognized promptly
([Aikten 2007](#ref-DiseasesOfSheep)). Many farmers will be faced with a few cases
annually, but in certain years up to 40% of ewes in a flock may be
affected. The natural incidence in intensively farmed sheep is
approximately 2% of pregnant ewes. Although, in case of severe
deficiencies in managing the disease, case-fatality rate can approach
100% in untreated cases. Even with early treatment, case fatality can
still be high ([Done et al. 2006](#ref-VeterinaryMedicine); [Radostis and al. 2006](#ref-ciao); [Kelay and Assefa 2018](#ref-PregnancyToxemiaEwe)).

In dairy goats PT occurs commonly, especially in specific breeds, like
Saanen and Alpine, which are genetically more prone to become pregnant
with multiple foetuses.

Does with ketonemia following kidding (i.e., ketosis) are more prevalent
than ketonemia before kidding (i.e., pregnancy toxemia). The herd-level
prevalence may vary between 0 and 18%; of these, the overall prevalence
of high-risk PT is around 10%.

Also, 4 weeks before kidding, BCS is associated with a greater risk of
developing PT in fat goats (14.4%) compared to thin (7.7%) and normal
goats (7.8%).

The overall mortality rate is 5.5% from prepartum and up to 1 week
postpartum. The herd-level mortality rate varies from 0 to 11.7%
([Dubuc, Bèlanger, and Buczinski 2015)](#ref-IncidenceGoats)).

Occurrence of sporadic, unexplained cases of pregnancy toxemia, even in
well-managed flocks, also suggests that genetic lines or families of
sheep and goats may be predisposed to metabolic disorders within a given
management system, and progeny from affected animals should probably be
excluded from flock replacements ([Rook 2000](#ref-VetClinNAmerica)).

The economic costs associated with this pathology are considerable if
the prevention plans are not observed. Major losses may relate to
([Littledike, Young, and Beitz 1981](#ref-KetosisMilkFeverGrass)):

- Laboratory analysis for the diagnosis of the pathology;  Costs for drugs (which may not be sufficient to prevent death even with aggressive treatments if carried out late);
- Loss of milk production as a result of hypogalactia;
- Loss of BCS following lipomobilization and anorexia (consequently devaluation of the carcass weight);
- Lower and inefficient reproductive performances, especially in cattle;
- Disposal of dams with recurring cases;
- Death of a significant percentage of livestock in lack of treatment;
- Fetal death/abortion;

It is important that veterinarians understand producer expenses and
their impact on pregnancy toxemia treatment decisions. Death from
pregnancy toxemia occurs very late in pregnancy when, to a great extent,
the annual cost of maintaining (wintering) that animal (except for
lactation diet) has already been incurred. Affected individuals fail to
generate an annual income from the sale of lambs, kids, milk, fleece,
hair, or as a cull animal ([Rook 2000](#ref-VetClinNAmerica)).

# Pathophysiology of PT

![Pathophisiology of ketosis](/posts/img/pregnancy_toxemia/NEB.jpg)

To fully understand why sheep and goats can become ill with pregnancy
toxemia, it is relevant to know that in dairy ruminants metabolic
priority varies depending on whether the animal is pregnant or not. In
the first few weeks of lactation or in the absence of pregnancy, it is
the udder that has all metabolic priority (for the lactation of the
unborn). Once pregnancy has been established, is the uterus that takes
over in priority.

In sheep and goats at the end of pregnancy, especially in the case of
multiple pregnancies, due to the smaller size of the rumen limiting the
ingestion capacity, the high energy requirements of the fetuses, stress
and any concomitant diseases will create the conditions for lowered
blood sugar and production of ketone bodies, which circulate in the
bloodstream and can increase to toxic levels thus triggering the onset
of pregnancy toxemia ([Fantini 2019](#ref-spieg)).

The principal cause of pregnancy toxemia is a disruption of glucose
homeostasis. About 70% of fetal growth occurs during the last six weeks
of pregnancy, and this growth causes a large demand for glucose by fetal
tissues. Both ketogenesis and ketolysis are regulated at the whole-body
level by the endocrine system, with insulin and glucagon playing a
central role in preventing and facilitating ketogenesis and ketolysis,
respectively.

Main focus for the organism to achieve is to keep glucose share
persistent in the ematic flow.

Glucose can enter cells through two mechanisms:

1. Insulin-dependent mechanism: in order to enter the cells, glucose needs insulin: the latter binds to specific receptors that open channels allowing glucose to cross the cellular membrane;
2. Insulin-independent mechanism.

In presence of low quantities of glucose, some structures take priority
of it by diverting its contents: CNS, mammary gland, blood cells and
fetuses (placenta).

If it is not possible to preserve blood sugar, the body implements a
series of metabolic processes aimed at restoring the glucose hematic
concentration from different substrates:

- Proteins: some amino acids can be transformed into pyruvate, others into oxalacetate and so on. The main purpose is to enter the tricarboxylic acid cycle in attempt to restore glucose via gluconeogenesis;
- Carbohydrates: they restore glucose through gluconeogenesis; this process is largely similar to anaerobic glycolysis except for a specific step: the transformation of pyruvate into Acetyl-CoA catalyzed by pyruvate dehydrogenase. This reaction is not reversible, thus Acetyl-CoA cannot be directly transformed in pyruvate and glucose.
- Lipids: their involvement is mainly related to the difficulty in maintaining energy production in the form of ATP. Their activation depends on both NEB and hypoglycemia. Firstly, lipids seek glucose supplies from glycogen deposits in muscles and liver; then glycogen is mobilized in order to provide glucose, which will be released into the bloodstream and keep the blood sugar constant. However, the liver has low glycogen storage, therefore it is not sufficient to maintain glycemia for more than a few hours. As a result, hyperglycemic hormones (cortisol, glucagon) and catecholamines are activated. These hormones stimulate triglycerides lipomobilization in the adipose tissue; TGs are broken into glycerol and long-chain fatty acids, and are conveyed to the liver in the form of NEFA or free fatty acids (FFAs) and glycerol. The mechanism of triglycerides exporting in ruminants is slow, leading to fat accumulation in the liver. This increases plasma concentration of NEFAs, whose uptake by the liver causes a steatosis, affecting parts of the organ's capabilities. In the liver:
  - Glycerol may be used to produce glucose or may be recombined with FFAs to make triglycerides (TGs).
  - Not degraded NEFAs are stocked in the hepatocytes in the form of triglycerides. However, the liver can trigger its lipotropic function (its ability to synthesize proteins) only if poorly involved in other processes. Otherwise, persistence or accumulation of TGs may cause fatty degeneration of the hepatocyte.
  - Mobilized FFAs can be degraded through $\beta$-oxidation and converted to acetyl-CoA;

Under theoretical conditions, Acetyl-CoA has three fates:

1. Enter the Krebs cycle, but in order to enter it requires the following oxaloacetate (oxaloacetate plus acetyl-CoA form citrate); In anorexic ewe/doe, depletion of oxaloacetate is common (since it is converted to glucose) thus the normal Krebs cycle and the use of FFAs is inhibited.
2. Steroids synthesis, used to produce cholesterol;
3. It accumulates, and is condensed and recovered for transformation into Aceto-acetate, first of the ketone bodies and precursor of all three ketone bodies.

Hence, a higher level of Acetyl-CoA is essential for ketogenesis, and is
obtained mainly through a high supply of free fatty acids, which can
lead to a surplus production of acetyl-CoA not entering the
tricarboxylic (TCA) cycle for ATP generation but leading to the
formation of ketone bodies (Aceto-acetate, B hydroxyl Butirate and
Acetone).

At the hepatic level, therefore, the following events are observed:

1. Increased concentration of NEFAs from lipomobilization of adipose tissues; they are recruited in proportion to their concentration and degraded, favoring glucides. In addition, fatty acids exert direct inhibition on the enzymes of glycolysis whence the slowing of the Krebs cycle. All these interrelated factors, predispose the accumulation of acetyl-CoA resulting in successive $\beta$-oxidations of NEFAs. Lipomobilization is worsened in lack of insulin while being stimulated by glucagon; there is massive production throughout the body of acetyl-CoA with no possibility of utilization except for partial KB formation in the liver, under stimulation of glucagon.

2. Intense neoglucogenesis: most of the oxaloacetate is involved in this pathway; as a result, all the acetyl-CoA cannot be used for the Krebs cycle, leading to the only remaining pathway: the condensation of acetyl groups to the formation of ketone bodies.

![](/posts/img/pregnancy_toxemia/biochem.svg)

![](/posts/img/pregnancy_toxemia/acetate.svg)

---

The initial step of ketogenesis is the formation of acetoacetyl-CoA,
which is derived from two acetyl-CoA by the enzyme thiolase.

The newly produced acetoacetyl-CoA is then combined with another
acetyl-CoA via a reaction with HMGCS2, hence forming HMG-CoA. This
reaction is the rate-limiting step, which is regulated by insulin and
glucagon, which inhibit and facilitate Hmgcs2 transcription via forkhead
transcriptional factor FOXA2.

The enzyme HMG-CoA lyase subsequently removes an acetyl-CoA from HMG-CoA
to produce the ketone body acetoacetate, which can either undergo
spontaneous degradation to acetone (another ketone body with minimal
metabolic contribution) or form the ketone body beta-hydroxybutyrate
(BHB), the most abundant ketone body in the circulation.

This occurs through an enzymatic reaction with beta-hydroxybutyrate
dehydrogenase (BDH), requiring a hydrogen donated from the NAD+/NADH
couple.

Acetone is a volatile ketone, thus eliminated by respiration; in
contrast the other two are ketoacids, thus highly hydrophilic and
eliminated along with body fluids, such as urine, mammary secretion, as
well as causing metabolic acidosis with lowered pH.

Acetoacetate and BHB are not utilized in the liver, as it lacks
important enzymes for ketolysis, but are instead released into the
circulation for metabolism in extrahepatic tissues by which skeletal
muscle ([Jensen et al. 2020](#ref-ketoneb)).

It is therefore established that following ketosis, the system of
metabolism hormonal regulation is entirely directed not only toward
lipomobilization but also toward intense neoglucogenesis, the latter
carried out predominantly in the liver. They are therefore called ketone
bodies:

- Aceto-acetic acid CH<sub>3</sub>-CO-CH<sub>2</sub>-COOH
- Acetone CH<sub>3</sub>-CO-CH<sub>3</sub>
- &#x03B2;-hydroxybutyrrate acid CH<sub>3</sub>-CHOH-CH<sub>2</sub>-COOH
- Isopropanol CH<sub>3</sub>-CHOH-CH<sub>3</sub>

![](/posts/img/pregnancy_toxemia/KB.svg)

They are very diffusible small molecules; they will be poured into the
circulating bloodstream as they are used increasingly by peripheral
tissues. Ketone bodies (BHB and acetoacetate) are also strong acids and
their accumulation in the blood leads to metabolic acidosis
(ketoacidosis), indicated by decreased blood pH.

The uptake of ketone bodies across the blood-brain barrier is
carrier-dependent and unlike glucose transport, not increased by
neuronal activity, but instead related to concentrations in the
circulation. Monocarboxylate transporters (MCTs) are the only known
transporters for ketone bodies and are distributed throughout the brain:
neurons, and to a certain extent also astrocytes, have the capacity to
take up ketone bodies.

Once BHB and acetoacetate have been transported into the brain, can
serve as fuel for the brain ([Jensen et al. 2020](#ref-ketoneb)).

In late gestation, ketone bodies see their catabolism precipitate,
parallel to that of glucose. All this is already latent during the early
clinical phase where acetate misutilization is already chronic; also the
liver increases gluconeogenesis to facilitate glucose availability to
the fetuses.

Each fetus requires 30-40 g of glucose/day in late gestation, which
represents a significant percentage of the ewe's glucose production and
which is preferentially directed to supporting the fetuses rather than
the ewe. Mobilization of fat stores is increased in late gestation as a
method of assuring adequate energy in the face of increased demands of
the developing fetuses and impending lactation.

This could be the consequence of increased adrenal output or reduced
excretion by the liver ([Kelay and Assefa 2018](#ref-PregnancyToxemiaEwe)). The breaking point
leading to the clinical form is difficult to locate. The main causes may
be as follows:

- Effect of ketone bodies (of high and sustained rate) on the central nervous system (CNS) and rumen-reticulum complex;
- Hypoglycemia in nervous forms was also impeached;
- Recurrent Acidosis could be responsible for digestive and nervous disorders;
- Significant rates of $\beta$-hydroxybutyrate, aceto-acetate, isopropanol and acetone can depress the CNS.

Therefore, starting from the occurence of the first symptoms of the
disease, the animal's feeding behavior only aggravates its nutritional
and metabolic state and the vicious cycle persists due to energy
deficiency and deficiency in *glucoformers* compounds, this results in a
decrease in the liver's oxaloacetate rate, and because of this, a
dramatic slowdown in neoglucogenesis.

As the disease progresses, there is not only a weak insulin rate but
also an absence of its increase following feeding, injection of glucose
or propionate, and a decrease in insulin-dependent tissue response.

There is also a positive correlation between blood glucose and cortisol,
and a negative correlation between ketone bodies and cortisol: this
suggests an important implication of corticosurenic activity in
ketonemia.

([Schulz and Riese 1983](#ref-Pathophisiology); [Boileau 2008](#ref-PregnancyMelanie))

## Pregnancy toxemia effects

When both glucose precursors and glucose derived from intestinal
digestion are insufficient, hypoglycemia occurs, leading to depression
of the central nervous system, giving rise to the known symptomatology
(nervous symptoms, coma).

In early stages of the disease, animals exhibit hypoglycemia, elevated
NEFA levels, hyperketonemia, and ketonuria.

In late form metabolic acidosis, renal failure hperazotemia and loss of
consciousness are observed; blood levels of $\beta$- hydroxybutyrate
increase (\> 1 mmol/l); fetuses may present in an advanced state of
decomposition; the liver is enlarged, pale, friable, and with a greasy
cut surface (hepatic steatosis of various stages) ([Toteda 2016](#ref-alim)).

Subclinical pregnancy toxemia is associated with a reduced immune
response in sheep, with adverse effects on both cell-mediated and
humoral immunity. Similar results have been obtained in studies in
goats, where it has been shown that the pre-parturition period is
associated with alterations in lymphocyte function and reduced immune
response, particularly evident in goats with tri/bigeminal pregnancies.

The reduction in immune responsiveness brought by PT can lead to
negative consequences both on ewes and goats health in lactation stage,
as well as on the health status and survivability of lambs and kids.

Ewes that had presented BHB values above 0.86 mmol/l during the last
weeks of gestation produced a lower amount of colostrum (-60%), compared
with ewes with normal values. In addition, the protective value of
colostrum was lower in ewes with subclinical toxemia. The decrease in
immune responsiveness determined in ewes by the state of ketosis, could
also adversely affect the rate of passive immunity of newborns and
predispose lambs and kids to become more vulnerable to neonatal
diseases.

In small ruminants, it also appears necessary to highlight possible
relations between metabolic-nutritional conditions of the final stage of
gestation and the state of defense against common parasites. These
species are in fact known to manifest a kind of preparatory
"relaxation" of the immune defenses against nematode parasites, which
results in an increase in the survival capacity of worms and an increase
in egg production both in the last stage of gestation and during
lactation ([B. Ronchi 2008](#ref-rispimmun)).

Mean duration of pregnancy is significantly shorter in ewes/does with
pregnancy toxaemia (145.75 days) than in healthy animals (148.42 days).

Incidence of dystocia (mainly related to expulsive deficiency) is also
found. The most common reasons for dystocia in ewes are incomplete
dilatation of the cervix, malpresentation of the foetus(es) and foetal
oversize; expulsive deficiency is likely a consequence of primary uterine
inertia. This may possibly be the effect of inadequate endocrinological
mechanisms leading to parturition, due to the impaired stimulation of
the foetal hypothalamo-pituitary-adrenocortical axis in growth-retarded
foetuses consequently to the maternal undernutrition or to the shorter
duration of pregnancy recorded in ewes with the disorder.

Postural abnormalities of the foetus may be shown (such as shoulder flexion and lateral head deviation).

Some dams are also likely able to develope mild metritis and retention
of foetal membranes: metabolic disturbances associated with energy
deficiency in pregnant ewes have effects on protein metabolism: this
could in turn affect the enzymatic pathways leading to proteolysis of
the cotyledons and consequently to retention of foetal membranes.

Increased incidence of perinatal mortality in offspring of ewes with
pregnancy toxaemia is likely the effect of intrauterine growth
retardation, as the consequence of reduced energy availability.

Pregnancy toxemia is also associated with post-partum mastitis and
metabolic acidosis ([Barbagianni et al. 2015](#ref-complications)).

## Differences between bovine Ketosis and ovine/caprine PT

In cows, ketosis typically occurs in early lactation.

Low insulin concentration may confer higher glucose priority to the
mammary gland. This is corroborated by the fact that glucose transport
into the gland does not require insulin.

Many peripheral organs may be less able to assimilate glucose, acetate,
and ketone bodies from blood because the uptake of these compounds is
mediated by insulin, the concentration of which decreases. Some
impairment of liver function would also be expected.

Because the hormonal environment in early lactation favors mobilization
of adipose tissue, one might speculate that in such cows the initial
step in the etiology of ketosis is mobilization of an excessive quantity
of NEFA and consequent development of a fatty liver. The infiltrated fat
then might impair hepatic gluconeogenic capacity. Alternatively,
appetite may be depressed in these obese cows so that negative energy
balance develops. This in turn would lead to rapid mobilization of fat
from the ample stores available.

While infusion of glucagon does increase hepatic ketogenesis in sheep,
the behavior of glucagon in bovine ketosis is unknown; but studies with
ruminants suggest that in these species any ketogenic role of pancreatic
glucagon may be muted.

The question arises on why all cows that experience negative energy
balance in early lactation do not become clinical. Presumably, this is
because most cows are able to tolerate this period without developing
any serious discrepancies between glucose supply and demand.
Nevertheless, it is probably true that all dairy cows in early lactation
show some indication of the same type of metabolic changes seen in
extreme form in cows suffering from ketosis.

Although it may remit spontaneously, bovine ketosis may affect
productivity adversely in other ways, for example by decreasing milk
yield and impairing fertility.

With or without treatment, the occurrence of clinical or sub-clinical
ketosis is likely to ensure that maximum potential milk production will
not be achieved. Whereas the most obvious effects of the disorder are
the immediate ones, there also may be long-term implications for
productivity.

Firstly, exposure to clinical ketosis may have had long term effects on
reproductive potential even after signs of ketosis had subsided.
Secondly, some degree of subclinical ketosis may have persisted up to
the time of insemination.

It is probable that any subclinical ketosis would have disappeared
completely in these cows at the time of the following insemination. The
importance of glucose status at this stage is emphasized by observing
that conception in cows occurs when blood glucose concentration is
rising but not when it's falling.

Impaired liver function could be a salient feature of "parturition
syndrome" in dairy cows. According to this theory, the parturition
syndrome is a complex of pathological changes in organ function and
structure that occurs in the cow in response to metabolic stress imposed
during the period from late gestation through to next conception. The
various disorders during this period are signs of the syndrome and
include not only metabolic disorders and diminished fertility but also
infectious diseases such as mastitis. In the latter case the suggestion
is that organ or cellular malfunction renders the animal less able to
resist infection ([Baird 1982](#ref-BovineKetosis)).

Hypocalcemia and ketosis, can lead to stasis of the gastro-intestinal
(GI) tract and subsequent GI and abomasal atony. Abrupt changes in diet
can also cause upsets that can in turn cause GI atony. Experts believe
that the void created within the abdominal cavity after parturition can
allow the abomasum to move more freely. Also, cows in the early
postparturient period are at higher risk of developing other conditions
such as metritis, mastitis, hypocalcemia, and ketosis
([Edmondson and Pugh 2009](#ref-FoodAnimalPractice)).

In ewes/does, PT generally occurs in the last month of gestation or 2-4
weeks after parturition.

Clinically, these cases are typically limited to older ewes and does
during their second or subsequent pregnancy and in dams carrying
multiple fetuses or one single large fetus (Schlumbohm and Harmeyer
2008). The disease is uncommon in dams pregnant with a single fetus or
in yearlings bred for their first pregnancy. In sheep and goats, unlike
dairy cattle, pregnancy ketosis is much more common than lactational
ketosis and occurs more commonly in highly prolific improved breeds
([Rook 2000](#ref-VetClinNAmerica)).

The following table shows the main differences between bovine ketosis
and Ewe/Goat pregnancy toxemia.

<table>
    <tr>
        <th></th>
        <th>Bovine</th>
        <th>Ewe/Goat</th>
    </tr>
    <tr>
        <th>Period</th>
        <td><ul>
            <li>Lactation (1-3 weeks before birth)</li>
        </ul></td>
        <td><ul>
            <li>Gestation (last month)</li>
        </ul></td>
    </tr>
    <tr>
        <th>Main predisposing factor</th>
        <td><ul>
            <li>Overfeeding</li>
        </ul></td>
        <td><ul>
            <li>Bi-trigeminal pregnancy</li>
        </ul></td>
    </tr>
    <tr>
        <th>Main complications</th>
        <td><ul>
            <li>Placental retention</li>
            <li>Dislocation</li>
            <li>Failure to return to estrus</li>
            <li>Partuition syndrome</li>
        </ul></td>
        <td><ul>
            <li>Ruminal stasis</li>
            <li>Moderate meteorism</li>
            <li>Nervous symptoms</li>
            <li>Reduced immune response</li>
            <li>Abortion</li>
        </ul></td>
    </tr>
    <tr>
        <th>Hypoglicemia source</th>
        <td><ul>
            <li>Lactation peak</li>
            <li>Anorexia</li>
        </ul></td>
        <td><ul>
            <li>Fetal energetic request</li>
            <li>Dysorexia</li>
        </ul></td>
    </tr>
    <tr>
        <th>Treatment</th>
        <td><ul>
            <li>Easy: self-limiting</li>
        </ul></td>
        <td><ul>
            <li>Very difficult: poor prognosis</li>
        </ul></td>
    </tr>
</table>

## Predisposing factors

Through the studies carried out on the analytical epidemiology of the
disease, it is possible to isolate some of the most common risk factors
and predisposing factors for toxemia in pregnancy
([Pauluzzi and Valent 1991](#ref-SindromeChetosica); [Kelay and Assefa 2018](#ref-@PregnancyToxemiaEwe); [Boileau 2008](#ref-PregnancyMelanie)):

Extrinsic predisposing factors:

- Nutrition: it can intervene in different ways:
  - Ketogenic regimes, i.e. not providing in the rumen suitable proportions of the different volatile fatty acids (VFA) such as silage (with too much butyrate and lactate) or roughage (too much acetate and not enough propionate); global lack of nutrition and deficiency in cobalt (including deficiency in vitamin B12 necessary for the metabolization of propionate); excess of protein and non-protein nitrogen driving an energy deficiency due to waste of energy for urogenesis (inducing functional hepatic insufficiency);
  - Abrupt changes in nutrition post-partum, which can induce a disturbance of fermentations or even secondary motor
    disturbances of the rumen-reticulum complex, causing a global lack of nutrients. Overfeeding during dry period: if lipomobilization is not properly controlled post-partum, it can lead to significant hepatic resentment;
  - It might also be noteworthy to pay attention to the use of rations of heterogeneous composition throughout the day: this is not physiological for the animal and can be identified as a nutrition related predisposing factor;
- Physical exercise: the disease is much more frequent in animals living in fixed housing; this highlights the physiological importance of physical activity. The metabolic aspect is twofold:
  - Ketone bodies (KB) tend to pile up, and muscular work can use them up to produce energy. Without proper exercise they keep on accumulating;
  - Muscle exercise allows the use of stock glucose: sufficiently prolonged exercise favors anaerobic glycolysis, releasing lactate into the bloodstream, therefore captured by the liver for neoglycogenesis;
- Body condition: either poor body condition, and low body weight or obesity are other predisposing factors for the onset of the disease. During late gestation, in case of obesity, the abdominal space is filled with accumulated fat and an expanding uterus. This subtracts space from the rumen, causing difficulties in consuming enough food to satisfy the energy requirements. Cattle with poor body condition also cannot eat enough to meet their own nutritional needs and the energy requirement of their fetuses: this is because susceptible thin dam are often fed inadequate rations, and, facing the lack of energy to meet increasing fetal demands, more body fat gets mobilized, resulting in ketone bodies production and hepatic lipidosis;

Intrinsic predisposing factors:

- Hereditary predisposition: the existence of which affects how the neuroendocrine system responds to the quality and quantity of the enzymatic patrimony; Morphological weight type: such as body condition score (BCS) higher than normal or state of malnutrition that can bring into primary ketonemia;
- Interpart: which, if too long, may represent a high risk factor;
- Bi-trigeminal pregnancies;
- Parity: Clinical cases are typically limited to older goats and ewes during their second or subsequent pregnancies. The disease is uncommon in maiden ruminants and increases in prevalence later on;
- Health conditions: they constitute important risk factors, even more if liver is involved in their evolution, since is already compromised during the physiological and reversible steatosis pregnancy that can turn towards a pathological state (secondary ketosis). It can be altered by infectious diseases (especially if subclinic), parasitism (strongylosis, dystomatosis, coccidiosis) and by common diseases of other origins;
- Diseases: presence of other diseases like foot rot and foot abscess can also influence the onset of pregnancy toxemia. Such conditions acutely curtail feed intake,and the animal turns in NEB;
- Concurrent hypocalcemia or metabolic acidosis;
- Insulin-resistant cattle: unable to regulate glucose homeostasis in late pregnancy. This increased tissue resistance to insulin is most likely an inherited trait, suggesting that PT may be very similar to insulin-dependent diabetes mellitus in humans or type II ketosis in ewes and does.

Occasional aggravating factors:

- Environmental stress: stressors such as cold weather and rain increase the energy demand of the pregnant ewes/does so that induces stress (acute) syndrome
- Social stress;
- Climate stress: inadequate microclimate (temperature, humidity, ventilation);
- Transport stress.

Triggering factors:

- Lactation peak: an additional stimulus, as glucose demands increase for an organism that is already in chronic deficiency due to the onset of lactation. Milk production reaches its maximum in the third lactation and in subsequent lactations;
- Pregnancy.

## Clinical signs

PT is most common during the last 2 to 4 weeks of gestation. Being able
to detect the animal's behaviour in early stages is of outmost
importance: as the disorder progresses, any attempt of treatment becomes
progressively more difficult and could not be of any effectiveness.

It has been shown that this condition carries a poor prognosis with a
reported case fatality rate above 80% ([Simões et al. 2020](#ref-AdvancesinAnimalHealth)).

Affected ruminants often present high or low Body Condition Scores
([Aikten 2007](#ref-DiseasesOfSheep)).

During the progress of the disease, many of the animal's discernible
signals may be barely visible or associated with other disorders, such
as hypocalcemia ([East 1983](#ref-SymposiumonSheepandGoatMedicine)).

The first detectable clinical signs can range in a variety of different
behaviors:

- Separation from the group;
- Refusal of food;
- Prolonged decubitus;
- Weight loss;
- Anorexia;
- Hypogalaxia (during lactation);
- Hypothermia;
- Bradisfigmia: slow pulse rate;

As the toxaemia progresses, the pathology becomes more severe and may
lead up to the onset of some nervous disorders such as:

- Blindness;
- Tremors (narrowed at first then systemic);
- Convulsions;
- Recumbency: animals that have become recumbent have a grave prognosis;
- Coma (with or without abortion);
- Death.

Ewes have a tendency to isolate themselves, gradually moving away from
the flock. They may display fatigue and difficulty in eating and
drinking, leading to progressive dehydration. In a few days, in
combination with marked weight loss followed by anorexia, they exhibit
signs of blindness. It is also customary to observe a decrease in
neurility when approached by operators, without any attempt to escape.
If forced to move, the animal struggles to move away from the obstacles
encountered along the way, presenting a condition called *ataxia*,
represented by the inability to coordinate during deambulation.

In late stages, tremors and lateral deviation of the head may be
observed in response to nerve stimuli. Muscle tremor often spreads to
the entire body leading into tonicoclonic convulsions. After each
seizure the ewe rises normally but still blind. In the periods between
convulsions, the animal can show abnormal postures including stargazing,
represented by a raising of the chin in dorso-caudal direction.

Coma is likely to be found in affected animals after 3-4 days, and in a
shorter time for animals with a high BCS value.

Following a lack of immediate intervention, abortion occurs. This event
is often followed by a clinical improvement in the animal's condition,
but toxemia caused by decaying fetuses often causes an abrupt relapse
([Done et al. 2006](#ref-VeterinaryMedicine); [East 1983](#ref-SymposiumonSheepandGoatMedicine)).

Fatality rate is still above 80% even when c-section is performed or
after the induction of kidding ([Simões et al. 2020](#ref-AdvancesinAnimalHealth)).

Goats are inclined to be more resistant to pregnancy ketosis, but are
still susceptible to trigger in case of obesity, underweight,
nutritional deficiencies or reduced food intake.

Among the first displayed signs it is possible to detect an increase in
aggressiveness towards conspecifics and operators, making handling
operations difficult. When forced to stand up following prolonged
decubitus, they urinate frequently ([East 1983](#ref-SymposiumonSheepandGoatMedicine)).
Other clinical signs may include depression, lack of appetite and
decrease in milk production if lactating. The goat's breath has a
characteristic acetone smell when there is a high level of ketones or
toxins in the blood. Does may also grind their teeth and moan
frequently. There is often a noticeable subcutaneous oedema of the
hindlimbs (which can also occur in the forelimbs). In late stages, they
may be presenting the same neurological distress as ewes, such as
occasional tremors and mostly stargazing. Feces becomes reduced and more
fluid. They eventually lie down and are unable to rise. Without early
intervention, death usually follows shortly ([Simões et al. 2020](#ref-AdvancesinAnimalHealth)).

![Clinical sign of a stargazing goat](/posts/img/pregnancy_toxemia/stargazing_goat.jpg)

![Clinical sign of Ataxia and in-coordination in ewes](/posts/img/pregnancy_toxemia/pecora2.jpg)

The following table shows the different symptoms of PT and their
occurrence in ewes and goats
([Simões et al. 2020](#ref-AdvancesinAnimalHealth); [Done et al. 2006](#ref-VeterinaryMedicine); [East 1983](#ref-SymposiumonSheepandGoatMedicine)).


<table>
    <tr>
        <th>Clinical sign</th>
        <th>Goats</th>
        <th>Ewes</th>
    </tr>
    <tr>
        <td>Hindlimbs/forelimbs oedema</td>
        <td>x</td><td></td>
    </tr>
    <tr>
        <td>Anorexia</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Ruminal atony</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Polypnea</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Recumbency</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Neurologic signs</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Dropped ears</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Encephalopathy</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Muscle tremor</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Blindness</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Stargazing</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Vocalizing</td>
        <td>x</td><td></td>
    </tr>
    <tr>
        <td>Reduced and fluid feces</td>
        <td>x</td><td></td>
    </tr>
    <tr>
        <td>Separation from the group</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Failure to come up for feeding</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Convulsions</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Drowsiness</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Depression</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Coma</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Difficulty in lambing</td>
        <td></td><td>x</td>
    </tr>
    <tr>
        <td>Heart rate above the reference range</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Respiratory rate above the reference range</td>
        <td>x</td><td>x</td>
    </tr>
    <tr>
        <td>Acetone smell</td>
        <td>x</td><td></td>
    </tr>
    <tr>
        <td>Aggressiveness</td>
        <td>x</td><td></td>
    </tr>
</table>

Studies show that 100% of goats with neurologic signs or drooped ears
dies. In addition, 94% of goats with anorexia, 90% of recumbent goats,
78% of goats showing polypnea and 72% of goats with swollen limbs also
died.

In ewes the course of the untreated disease varies from 12 h to one
week.

In goats 50% of the females have been shown to die in the first 24 h
even with very aggressive treatments ([Simões et al. 2020](#ref-AdvancesinAnimalHealth)).

## Diagnosis and differentials

Pregnancy toxemia should be suspected whenever a late pregnant ewe or
doe appears sick, especially when farmers have any history of
late-pregnant animals exhibiting signs of weakness followed by death in
3-10 days ([Heller n.d.](#ref-MeeraHeller)).

For an accurate diagnosis of pregnancy toxemia, a differential diagnosis
is important to distinguish this metabolic disorder from others with
similar signs.

Differentials may include a series of pathologies of which clinical
signs are mostly similar to PT
([Erickson 2020](#ref-Differentials); [Boileau 2008](#ref-PregnancyMelanie); [Marteniuk and Herdt 1988](#ref-PTJudith); [Anderson and Rings 2009](#ref-pipi); [Fecteau and L. W. George 2009](#ref-polioenc); [Smith and Sherman 2007](#ref-moro); [Solaiman 2010](#ref-magnes)):

- Scrapie: a degenerative neurological disorder caused by an
  infectious agent referred to as a prion. Goats are far less commonly
  affected than sheep. Long periods of incubations are observed
  (ranging from 30 up to 146 weeks). Replications of prions occurs
  first in lymphoid tissues and subsequently in nervous tissue, where
  progressive neuronal degeneration is responsible for the clinical
  signs observed. Affected animals may present behaviour changes,
  tremors, ataxia, constant scratching and evidence of wool pulling;
- Rabies: a fatal disease affecting ruminants worldwide, caused by
  Lyssavirus of the Rhabdoviridae family; the latter migrates to the
  brain through peripheral nerves. Once the neurologic signs appear,
  the course of the disease is invariably fatal. Two clinical
  presentations exist: furious and dumb. Furious rabies produces
  behavioral changes that include rage, hyperexcitability, tonic
  clonic convulsions, vocalization and sexual excitement. Dumb rabies
  present profound depression, ataxia, loss of appetite, distant
  stare, paralysis. Clinical patterns in some animal may shift from
  the furious to the dumb forms within hours.
- Cerebral abscess: there are many causes of cerebral abscess,
  including bacterial or fungal infections. Main symptoms may include:
  weakness and involuntary movements (such as head tilts), vision
  loss, depression, coma.
- Acidosis: occurs when animals consume large quantities of grain or
  pellets to which they are unaccustomed. Main symptoms include:
  downer sheep, bloated, weak, coma, death;
- Polioencephalomalacia: caused mainly by thiamine deficency
  (necessary for brain's glucose metabolism, GM). Altered GM leads to
  celebral edema and necrosis. Affected animals are depressed and
  anorexic, neurological signs may be observed, such as blindness,
  ataxia, and as the disease progresses recumbency and death few days
  later.
- Listeriosis: is the designated term for infections associated with
  Listeria monocytogenes. The pathology is associated with ingestion
  of contaminated soil, silage or water. Clinical signs of the disease
  can manifest up to 3-6 weeks after bacterial inoculation. Animals
  are usually anorectic and depressed. Increased heart respiratory
  rate may be observed as well as fever. Dams become hemaciated, with
  a severe decrease of rumen motility, may present ataxia, seizures
  and recumbency.
- Bacterial or viral encephalitis: It is reported to be highly fatal.
  After an upper respiratory viral growth, migration towards CNS
  occurs. Clinical signs may include depression and/or excitement,
  head pressing, aimless circling or ataxia and convulsions.
- Hypomagnesemia or Grass Tetany: is a metabolic disorder associated
  with low magnesium concentrations in blood. This condition is most
  prevalent when animals are milking heavily and have a higher
  requirement for magnesium or early in spring when animals are
  grazing rapidly growing pastures that are heavily fertilized. Grass
  tetany occurs when pastures are low in magnesium but rich in
  nitrogen and potassium that result in a low magnesium/potassium
  ratio.In the acute stage of the disease, many symptoms may be
  displayed, such as: excitement, tremors, twitching of facial
  muscles; hyperaesthesia, aimless wandering, falling over,
  convulsions, death; in subclinical stages: inappetence,
  apprehension, milk yield decrease, mild ataxia, may convulse in
  response to noise or handling; may be spontaneously recover or
  progresses to acute phase. In chronic stages affected herd may show
  poor growth, reduced milk yield and dullness due to low serum
  magnesium levels. The condition may be chronic and if undetected
  will predispose an animal to milk fever
- Milk fever or Hypocalcemia: is a non-febrile disease of ruminants,
  associated with acute calcium deficiency causing progressive
  neuromuscular dysfunction. Ewes may present milk fever in late
  gestation rather than the onset of lactation. Its main signals
  include flaccid paralysis muscle tremors, ataxia and recumbency. In
  goats milk fever may occur either in prepartum or postpartum.
  Clinical signs are similar to those in sheep and include both
  hyperestesia with tetany and flaccid paralysis. Most clinical cases
  of hypocalcaemia occur after kidding when the increased demands of
  calcium for lactation cannot be met quickly enough, but
  hypocalcaemia may occur in conjunction with pregnancy toxaemia and
  subclinical disease may be present at any stage of the
  periparturient period. Many goats will benefit from calcium therapy
  when presented with other periparturient diseases and all recumbent
  or comatose goats should be treated as potentially hypocalcaemic and
  given calcium. Hypocalcemia and PT can be differentiated based on
  serum calcium concentrations, which are usually below 6 mg/dl in
  cases of primary hypocalcemia. Response to treatment for
  hypocalcemia is usually rapid, whereas response to treatment for
  pregnancy toxemia is often unrewarding. Diagnosis of lactation
  ketosis is based on the stage of lactation, physical examination,
  and the presence of ketone bodies in urine or milk. Affected
  individuals are usually in the first 2 months of lactation. A
  history of insufficient energy and/or starch supports the diagnosis.

Altough they often may be seen together and may exploit almost the same
clinical symptoms, there are important differences between Hypocalcemia
and Pregnancy toxemia, as shown in the table below:

<table>
    <tr>
        <th></th>
        <th>Pregnancy toxaemia</th>
        <th>Hypocalcaemia</th>
    </tr>
    <tr>
        <th>Causes</th>
        <td><ul>
            <li>Low levels of glucose in the blood</li>
            <li>Increasing metabolic demand of pregnancy</li>
            <li>Nutrition not meeting demand</li>
            <li>Pregnant ewes feeding predominantly on green pick are at increased risk</li>
            <li>Can be associated with yarding or transport</li>
            <li>Can occur following severe weather</li>
        </ul></td>
        <td><ul>
            <li>Low levels of calcium in the blood</li>
            <li>Can be secondary to eating plants containing oxalates</li>
            <li>Nutrition not meeting demand</li>
            <li>Pregnant ewes feeding predominantly on green pick are at increased risk</li>
            <li>Can be associated with yarding or transport</li>
            <li>Can occur following severe weather</li>
        </ul></td>
    </tr>
    <tr>
        <th>Signs</th>
        <td><ul>
            <li>Ewes separated from the mob</li>
            <li>Drowsy or comatose</li>
            <li>Stop eating</li>
            <li>Nervous signs: tremors, ataxia, blindness</li>
            <li>Go down and lie on their side for 3-4 days</li>
            <li>Death 3-4 days later.</li>
        </ul></td>
        <td><ul>
            <li>Rapid onset</li>
            <li>Often a number affected</li>
            <li>Stiff, uncoordinated gait</li>
            <li>Nervous signs: muscle trembling, paralysis</li>
            <li>Down ewe sitting on her brisket unable to get up</li>
            <li>Death within 24 hours.</li>
        </ul></td>
    </tr>
    <tr>
        <th>Post mortem changes</th>
        <td><ul>
            <li>Twin lambs often found</li>
            <li>Yellow liver</li>
        </ul></td>
        <td><ul>
            <li>No significant findings</li>
        </ul></td>
    </tr>
</table>

After observing clinical signs, the easiest way to confirm a suspicion
of pregnancy toxaemia is through radiography, ultrasonography, urine and
milk and/or blood samples to detect ketone bodies.

A radiography may show multiple fetuses via examination of lateral
abdomen view.

Ultrasonography of the liver in pregnancy toxemia cases may appear more
echogenous than a normal liver parenchyma. The hyperechoic features may
be attributed to changes in the nature of the liver tissues that
increases the attenuation of the ultrasound beam.

Glucometers may be useful tools to detect hypoglicemia. Glucometers are
portable, convenient, and simple-to-use devices used to check blood
sugar levels anywhere and anytime. These devices analyze a small amount
of blood, and provide results within seconds. Glucose concentration is
determined immediately after blood collection from the jugular vein;
after collecting the blood sample, a strip is used and dipped in the
blood sample and therefore inserted in the glucometer that gives back
the result of blood value.

The urine ketone tests are semiquantitative tests based on the degree of
colour change that occurs when sodium nitroprusside reacts with
acetoacetate and, to a lesser degree, acetone. Sheep and goats can be
induced to urinate by temporarily closing off their nostrils. Does have
been shown to often urinate when forced to stand up. Ketones are
detectable in urine with the nitroprusside test. The latter is highly
sensitive to acetoacetate, slightly sensitive to acetone and insensitive
to beta-hydroxybutyrate (BHB). In ruminants the ratio of BHB to
acetoacetate is normally greater than 10:1, so the concentration of
ketone bodies determined by the urine dipstick is always underestimated.
If aciduria and ketonuria are shown, they might be due to the increasing
glucose demand that the dietary supply cannot suffice in pregnancy
leading to the increased mobilization of long chain fatty acids from
adipose tissues and a marked rise in circulating non esterified fatty
acid and ketone bodies, which in turn, descend in urines.

Rothera is commonly used to detect KB, glucose and pH level in urine. It
requires a urine specimen sample that will be poured into a test tube
along with sodium nitroprusside (0.75 gm) and ammonium sulphate (20gm).
If an immediate formation of purple permanganate colored ring is
observed at the interface, then ketone bodies are present (Positive); if
no formation of purple permanganate colored ring is observable at the
interface, ketone bodies will be absent (Negative) ([Giri 2018](#ref-rotera)).

A more accurate strategy consists in measuring BHB in blood, which in
pregnancy toxaemia goats should be above 3 mmol/L. Determining the level
of BHB in blood is considered to be the 'gold standard' for monitoring
and identifying subclinical ketosis pre-kidding. BHB is a more reliable
indicator of disease severity than blood glucose levels (BGL). BGL in
pregnant goats affected with pregnancy toxaemia varies dramatically:
hypoglycaemia might indicate that the foetuses are alive and
hyperglycaemia that the foetuses are dead. Therefore, the assessment of
glucose levels in field conditions using a glucometer would be an useful
quick test for practitioners in order to evaluate the status of the
foetuses. The situation can progress to an irreversible stage, where
dehydration and increased blood urea nitrogen (BUN) values are shown.

For estimation of hematological parameters, complete blood count (CBC)
may be assessed with an automatic cell counter:

- Increased TLC might be due to metabolic acidosis (ketoacidosis), infections, localized inflammatory processes and hepatic tissue necrosis;
- Neutrophilia could be due to hepatic lipidosis in which exposure of hepatocytes to fatty acids elicits inflammation, increases oxidative stress, apoptosis and production of fibrogenic cytokines;
- Lymphopenia may be caused by a toxic increase of BHBA and acetoacetate which leads to inhibition of caprine lymphocytic proliferation and reduced caprine T-lymphocyte blastogenesis;
- Increased blood BHBA level in does PT could be attributed to disturbances in carbohydrate and fat metabolism leading to an increase of lipolysis of fat tissues and release of long chain fatty acids that are converted by the hepatocytes to ketone bodies through the epatic ketogenesis process;
- Hypoglycemia may be related to increased BHBA in NEB, which causes hypoglycemia;
- Hypocalcaemia might be associated to higher needs of Ca for fetal skeleton development;
- Decreased serum total protein count might be due to liver function impairment;
- Low serum cholesterol level could be caused by a fatty infiltration in the liver and a low output of lipoproteins.

([Simões et al. 2020](#ref-AdvancesinAnimalHealth); [Matthews 2016](#ref-diseasesofthegoat); [Brahma et al. 2019](#ref-diagnosis)).

# Prognosis and therapy

## Prognosis

Prognosis is prone to be poor if cattle develop clinical PT early in
pregnancy (\>3 weeks before due date), have prolonged reduced feed
intake and rumen motility, are severely depressed and recumbent, have
stillborn fetuses, and have low serum potassium or high BHBA levels.

The prognosis may range from fair to good if dams develop clinical PT in
late pregnancy (within 7 days of due date), are still ambulatory, are
not completely anorexic, and deliver live fetus(es)
([Boileau 2008](#ref-PregnancyMelanie)).

## Treatment

Treatment of pregnancy toxemia involves increasing the energy and
glucose supply to the animal and correcting secondary abnormalities such
as acidosis and dehydration. Treatment is often not successful,
especially if the dam is recumbent. Treatment regimens are expensive and
time consuming; the value of the animal and the owner's wishes should be
taken carefully into consideration before initiating therapy
([Marteniuk and Herdt 1988](#ref-PTJudith)).

Treatment goals include correction of ketosis, NEB, hypoglycemia,
hypoinsulinemia, hydration status, electrolyte imbalances, metabolic
acidosis, stimulation of appetite and feed intake, and removal of source
of energy demand (fetuses) if necessary. If treated early, affected
animals generally respond favorably. Once recumbent, response to therapy
is mostly unfortunate.

In cases of mild ketosis with normal feed intake or partial anorexia
(ruminants that are still eating, ambulatory, decreased appetite for
grain, and few CNS clinical signs) can be treated on the farm.

Traditional therapy focuses on reversing the negative energy balance by
stimulating the appetite, raising blood glucose levels, and treating
dehydration.

Females should be offered a palatable, energy-rich, highly digestible
feed with products such as B vitamins, rumen stimulants (supplementation
with vitamin B complex and transfaunation with rumen liquor may help
stimulate appetite), better quality roughage and increasing the amount
of concentrates.

Producers with ewes or does exhibiting early signs of pregnancy toxemia
often initially drench affected animals with 100 to 200 mL of propylene
glycol (to increase blood glucose) twice daily for several days (or
15-30 ml every 12 hours) until appetite returns. Propylene glycol or
glycerol can be used as the sole treatment in mild cases or can be used
as a supplement to more aggressive therapy in severe cases. Females may
be treated with oral or intravenous glucose, in addition to balanced
electrolyte solution that includes calcium potassium chloride (10-20
mEq/L), 5% dextrose, and sodium bicarbonate (sodium bicarbonate may be
administered to help counteract acidosis). Antibiotics can be
administered to prevent pneumonia ([Edmondson and Pugh 2009](#ref-FoodAnimalPractice); [Marteniuk and Herdt 1988](#ref-PTJudith)).

Some authors believe a smaller dose (60 mL twice daily for 3 days) of
propylene glycol is more appropriate and less likely to cause side
effects. Mildly affected animals often respond to simple treatment and
continue their pregnancy to lamb or kid
([Boileau 2008](#ref-PregnancyMelanie); [Rook 2000](#ref-VetClinNAmerica)).

In cases of moderate to severe ketosis with partial or complete
anorexia, small ruminants that do not respond or deteriorate despite
24-48 hrs of therapy administered in the field should be referred to a
veterinary clinic or a teaching hospital.

The erratic response to therapy observed by early investigators may have
been a result of a failure to evaluate individual cases for hydration,
serum electrolyte concentrations, renal function, and acid-base status.
These conditions vary between cases, even with similar clinical signs,
and are likely to influence the outcome of therapy.

In the later stages of disease, when the animal is recumbent, the
prognosis is poor and treatment must be aggressive. Treatments must be
initiated immediately, and removal of the fetuses is crucial.

Before starting treatment, it should be determined whether the fetuses
are alive (eg, via real-time or Doppler ultrasonographic examination).

If the fetuses are dead or too premature to survive a cesarean section,
it is less stressful to the ewe or doe to induce early parturition with
dexamethasone. Antimicrobial therapy (usually procaine penicillin G at
20,000 U/kg, IM, every 24 hours for up to 5 days) is appropriate if the
fetuses are thought to be dead ([F. George 2022](#ref-treatment)).

If the fetuses are alive, is essential to reduce the ewe's glucose
requirements, either by cesarean section or by artificially inducing
parturition using corticosteroids.

Since a breeding date is rarely known, age of the fetuses is difficult
to determine. Fetal viability and survival is best if the dam is induced
within 5-7 days from gestational term. Depending on response to therapy,
owner's preference, and economical constraints, induction of parturition
or emergency c-section (left paralumbar fossa) can be performed. To
induce parturition in a doe:

- 10mg of prostaglandin F2$\alpha$;
- 20mg of dexamethasone IM.

Kidding should be expected within 30-36hrs of induction. In ewes,
lambing is expected within 48-72 hours following administration of 20mg
of dexamethasone IM.

Before initiating treatment, hydration should be assessed by PCV; serum
electrolyte concentrations, especially calcium and potassium, should be
determined; renal function should be assessed via blood urea nitrogen or
creatinine concentrations; and acid-base status should be evaluated by
serum total CO 2 concentration. In addition, blood glucose
concentrations should be determined, because many ewes with pregnancy
toxemia become hyperglycemic after the onset of clinical signs. Complete
blood counts are also helpful, because they may suggest the presence of
sepsis. Correction of dehydration, electrolyte abnormalities, and
metabolic acidosis should be attempted by appropriate fluid therapy
([Edmondson and Pugh 2009](#ref-PTJudith)).

In more severe cases, aggressive fluid therapy, including dextrose and
bicarbonate for ketoacidosis is necessary to replace the glucose and
electrolyte loss, as well as to rehydrate and at the same time to flush
out the ketone bodies. Potential metabolic complications include
persistent hyperglycemia, hypokalemia, dehydration or
overhydration/hypervolemia. Solutions with dextrose should be
administered judiciously to avoid inducing glucose diuresis.

Fluid therapy is recommended for
([Boileau 2008](#ref-PregnancyMelanie); [Matthews 2016](#ref-diseasesofthegoat); [Edmondson and Pugh 2009](#ref-PTJudith)):

**Dehydration:**

Any fluid deficits from dehydration should be corrected first, using a
balanced isotonic electrolyte solution. The fluid deficit can be
calculated using the formula:

Body weight (kg) $\times$ Dehydration (%) = Fluid Deficit (L)

To reduce chances of developing cerebral edema, is important to avoid
overhydration and limit the rate at which blood glucose (BG) level
drops.

Since serum lactate levels can be elevated in dehydrated cattle with PT,
veterinarians should try to avoid lactated ringers. Somministration of
is recommended only if severe metabolic acidosis is present (blood pH \<
7.0).

**Negative energy balance:**

NEB can be corrected by adding dextrose and amino acids to the isotonic
electrolyte solution:

5L balanced isotonic fluid + 500ml 50% dextrose + 1L 8.5% amino acids

It should be administered at constant fluid rate:

Fluid solution = 5% body weight (1kg = 1L)

That is about 50 ml/kg/day or 2.5 litres/day for a 50 kg animal. 20 mmol
of potassium can be added per liter of fluid to help prevent hypokalemia,
at a maximum infusion rate of 0.5 mmol/kg/hour.

**Provide glucogenic agents:**

- 20% Glucose solution, 200 ml
- Glycerine (glycerol), 60 ml in warm water orally, twice daily for 4 to 5 days.
- Ketocol (Leeders) is a commercial preparation of glycerol, which is more palatable than propylene glycol, 50 ml in 500 ml of warm water or by drench twice daily, given as a bolus rather than sprayed on feed.
- Propylene glycol, 200 ml orally twice daily for 4 to 5 days. In addition, propylene glycol also inhibits rumen bacteria to prevent production of volatile fatty acid ([Jesse et al. 2016](#ref-previ)).

Lactation ketosis may be treated with intravenous injections of 250 to
400 ml of 10% glucose solution in combination with propylene glycol
given orally. Glucocorticoids may also be administered but are
frequently unnecessary. If ketosis is secondary to another disease,
correction of the primary disease must accompany treatment of ketosis.
The use of glucocorticoids may be contraindicated if ketosis is
accompanied by an infectious disease. In cases of lactation ketosis, the
diet must be improved after treatment, or the animals will relapse.

**Stimulate gluconeogenesis:**

- Dexamethasone, 25 mg, 12.5 ml i.m. (will produce abortion in late pregnancy).
- Dexamethasone, 25 mg + protamine zinc insulin, up to 40 U, s.c., twice daily. Insulin has an antilipolytic effect as well as affecting peripheral glucose utilisation. Higher doses of dexamethasone (25 mg) may also be beneficial, perhaps by protecting the animal against the metabolic effects of acidosis and hypovolemia.
- Anabolic steroids
- Flunixin meglumine. Adding flunixin (2.5 mg/kg, i.m. SID) to the standard protocol for the treatment of pregnancy toxaemia may improve feed intake and the success rate for the delivery of live kids and the survival of the dam. The mechanism of action is unknown.

**Ketoacidosis:**

Goats with pregnancy toxemia, especially those showing neurologic signs,
should be suspected of having ketoacidosis. The acidosis should be
corrected using sodium bicarbonate according to the formula:

Body weight(kg) $\times$ base deficit $\times$ 0.3 = bicarbonate
needed(mmol)

Where 0.3 is the extracellular fluid volume (ECFV) for an adult goat.
Then 1 ml of 1.26% sodium bicarbonate solution = 0.15 mmol of
bicarbonate and can be administered at a rapid rate without problems. If
the serum bicarbonate level is not known, the correction must be
empirical. In severe acidosis, a base deficit of 10 mmol/l can be safely
assumed.

**Mild acidosis:**

Can be treated by dosing with sodium bicarbonate (500 ml of a 5%
solution). Small doses of sodium bicarbonate (100 to 150 ml) will
stimulate the oesophageal groove to close, aiding the oral
administration of other fluids.

**Provide supplemental Ca<sub>2</sub><sup>+</sup>, Mg<sub>2</sub><sup>+</sup>, P<sup>-</sup>, K<sup>+</sup> and via oral paste or gel as well as vitamins B complex.**

**Hypocalcemia:**

May coexist in goats with pregnancy toxemia. Calcium can be supplemented
at 25 ml of calcium borogluconate per litre of fluids or alternatively
80-100 ml of calcium borogluconate 20%, with magnesium hypophosphate 5%
and dextrose 20%, can be given i.v. Calcium should not be added to fluids
with sodium bicarbonate is in them. Fruiflavoured antacid tablets provide
a ready, palatable source of oral calcium carbonate, which can be used
to increase the calcium intake of less severely affected does. The
suggested dose is 1 g/50 kg body weight every 12 hours.

If a ewe or doe is already comatose, euthanasia is warranted, and
treatment should focus on the rest of the flock ([F. George 2022](#ref-treatment)).

## Monitoring

It is mandatory to evaluate BG, ketonuria and glucosuria at least 2-4
times a day, not only within the first 2-3 days of hospitalization but
also throughout the entire treatment period and to adapt therapy as
needed.

Once the patient is stable, frequency of monitoring can be decreased to
1-2 time/day.

It is essential to monitor acid-base status via arterial or venous blood
gas, serum creatinine, and electrolytes (especially K+) every 1-3 days.
Metabolic acidosis increases ECF K+ levels, and dextrose administration
combined with insulin lowers them.

Assessment of fetal viability with ultrasounds (1-2x/d) helps with the
decision to induce parturition (live fetus) or perform a c-section
(dam's condition severe; dead fetus). The normal fetal heart rate in
dams ranges from 130-160 bpm.

## Postmortem Examination Findings

Necropsy findings vary according to the body condition of the ewe at the
onset of disease and the initiating cause. The carcass can appear
emaciated or in good condition. Sporadic cases of pregnancy toxemia are
often defined by concurrent disease conditions having little to do with
feeding or management practices; however, as a rule, the uterus of most
affected individuals contains multiple fetuses in various stages of
decomposition if fetal death occurred prior to maternal death.

Overconditioned individuals typically exhibit severe fatty degeneration
of the liver (enlarged, friable, and pale yellow in color) which occurs
as the body mobilizes large quantities of fat depots; however, this
finding alone is not sufficient for a diagnosis of pregnancy toxemia.
Fatty infiltration of the liver is in fact a normal event during late
pregnancy. Elevated hepatic fat levels (normal 3% fat, elevated up to
30% fat) are an expected occurrence during late pregnancy.

Additionally, during postmortem examination, another lesion often seen,
although not consistently, is adrenal gland enlargement. In some
instances, they may be greater than 65% larger than normal. This
enlargement of the gland is a secondary hypertrophy that occurs as the
gland is stimulated to produce more cortisol in an attempt to raise
blood glucose levels. In addition, the adrenal cortex may appear darker
in color, while the medulla lighter than normal.

Other lesions that are occasionally seen include pale, fatty kidneys and
heart, and distended mesenteric blood vessels. Signs of constipation are
also common.

None of the above lesions by itself is pathognomonic for pregnancy
toxemia. However, when seen in combinations of several or all of the
above, these findings are highly suggestive of pregnancy toxemia,
especially if the lesions are seen in a carcass that is either emaciated
or obese.

In contrast, thin ewes or does may exhibit few of the above signs. Thin
animals typically exhibit necropsy signs consistent with starvation.
These include serous atrophy of perirenal and cardiac fat and enlarged
adrenal glands. A large single fetus is not an uncommon necropsy finding
in very thin ewes or does.

Histopathologic investigation may show neuronal necrosis along with
astrocytic nuclear swelling, hypertrophy, and proliferation. These
findings support the theory that clinical signs of pregnancy toxemia
result from a hypoglycemic encephalopathy
([Boileau 2008](#ref-PregnancyMelanie); [Rook 2000](#ref-VetClinNAmerica); [Schulz and Riese 1983](#ref-Pathophisiology)).

# Field Management and Prevention

PT constitutes a frequent and serious disease of prolific females in
late pregnancy, leading to high mortality of does and loss of valuable
offspring. Veterinarians can play a vital role in preventing losses from
pregnancy toxemia by training owners to acknowledge management factors
predisposing to disease, and by teaching them to prioritize observations
and develop management strategies to decrease the danger of pregnancy
toxemia within the herd.

Utilization of ultrasound for pregnancy diagnosis, including staging and
enumeration, accurate identification and recordkeeping, are the
cornerstones of those strategies. Monitoring body condition, adjusting
the timing and amount of lead-feeding based on risk and closely monitor
the highest-risk animals, while providing a system for early detection
and treatment of clinical disease, are the essential key to minimizing
the impact of pregnancy toxemia within the goat herd ([Rowe 2014](#ref-prevent)).

## Management strategies

*Accurate animal identification*

This is crucial for preventing and establishing a control program
against disease outbreaks, especially when management decisions have to
be considered on an entire group of animals; an incorrect or incomplete
identification system is a major risk factor for the farm, as it could
jeopardize proper grouping of animals or inadequate feed
administrations; in particular sensitive conditions, it could be the
cause of iatrogenic abortions as a result of incorrect prostaglandin
dosage. Attention to detail will maximize the usefulness of records data
([Rowe 2014](#ref-prevent)).

Animal identification systems can be of various types, but all must meet
the requirements of immovability, durability, legibility, be
inexpensive, not harmful to the animal, and be compatible with farming
systems. Depending on the species, the authorized identifiers are:
branding eartag, tattoo, electronic identifier (ruminal bolus,
injectable microchip, ear tag). The identification tool carries the code
of the animal, which can be either unique or holding. The means of
identification are generated for the holding. The means of
identification once applied to the animal cannot be removed, otherwise
sanctions will be imposed on the farmer ([Bortolotti 2019](#ref-identif)).

*Watching for concurrent diseases*

There are several diseases that cause weight loss and compromise
sustained energy balance in late gestation; these, if not carefully
recognized and treated, can be an important compromising factor in the
development of toxemia even in low-risk individuals. It is necessary for
owners to keep a watchful eye for the occurrence of differential
diseases manifesting the same clinical symptoms as toxemia, and to check
for the concomitant presence of endo and ectoparasitic diseases by fecal
eggs counts and using deworming agents if necessary.

In adult animals, it is also advisable to frequently observe the
possible occurrence of dental disease conditions (e.g., loose teeth,
infected tooth roots), as they are more susceptible.

Optimal monitoring exercised for prevention, can significantly reduce
the risk of pregnancy toxemia and optimize transition of does to
successful lactation ([Rowe 2014](#ref-prevent)).

*Adjusting decrease mobility and exercise*

Routine hoof trimming before late gestation will help does maintain the
agility and mobility needed to maximize time eating, prevent injuries,
and maintain social standing in the herd ([Rowe 2014](#ref-prevent)).

Providing physical exercises where possible can lead to greater benefits
for the cattle as it allows for an overall improvement of the
cardiovascular, respiratory and muscular systems. It also enables them
to maintain better endurance during calving and fetal expulsion.
Childbirth proceeds faster than in a sedentary dam, and with fewer
complications of dystocias and cesarean sections; in contrast,
overweight individuals have greater difficulties at the time of
expulsion as the fatty layers reduce the space of the uterus and birth
canal, leading to longer deliveries and possible complications (and, in
worsen scenarios, even to the death of the fetus or mother). Among the
various exercise plans, it is advisable to consider ([Krebs 2022](#ref-esercizio)):

- Animal density per available space: overcrowding can be a major obstacle and limiting factor on how much they can actually walk;
- Arrange resting, feeding and watering areas and recreation areas in different places in the facility, so that the animal can engage at least in a short walk to reach the various areas;
- If heavy snowfall is expected, be sure to clear the walkway and make it accessible and safe, especially from the risk of slipping.

*Confirm by ultrasound breeding dates and litter size*

Confirmation of pregnancy to a specific breeding date is critical for
managing the dry period, late gestation, and kidding. The doe should be
ultrasounded (or other confirmatory test used) to confirm pregnancy
status and stage of gestation.

Blood tests for pregnancy may be used to confirm pregnancy;
ultrasonography may allow determination of gestational stage, fetal
number, and fetal well-being (or impending/recent abortion), as well as
provide an opportunity for the veterinarian to detect risk factors or
early signs of disease.

Ultrasound staging of pregnancy is used to confirm gestation dates in
hand-mated herds and to estimate conception dates (to plan kidding
management) in pasture-penbred herds or groups. Staging of pregnancy is
most accurate early in gestation (35 to 75 days).

Pregnancies greater than 95 days gestation are too advanced to allow
accurate staging, and are categorized as "advanced." Gestational age can
be subjectively assessed based on size of fetus and placentomes, or the
size of the amniotic vesicle in early gestation.

Parietal diameter measurements can be used if indicated. Accurate
determination of conception date is essential for herds using induction
of parturition.

Fetal enumeration (counting) is most accurate when performed early in
gestation (40 to 75 days). Pregnancies greater than 90 days allow
reporting of multiple fetuses when observed, but are too advanced to
provide consistent results.

Enumeration exams are usually categorized as single, twin, and
triplets-or-greater pregnancies. If expected litter size (fetal
enumeration) has been determined, does can be grouped by fetal number
(and dam body condition) and fed appropriately in late gestation to
maximize energy intake and minimize risk of pregnancy toxemia. Disparity
between ultrasound predictions of litter size and number of kids
actually born may help to define embryonic death and presumed abortion
losses. Does with triplet-or-greater expected litters and problem does
(age, mobility) can be supplemented or started on lead feed at 4 to 6
weeks before kidding instead of the more usual 2 to 4 weeks before
kidding, or can be provided increased supplementation and frequency of
feeding, depending on their age, health status, and body condition
([Rowe 2014](#ref-prevent)).

*Add visual identification to high-risk does and other does requiring
closer observation*

Does may be grouped according to parity, BCS and gestational stage in
order to assess well-being by risk group; it is possible to use the
other does as cohorts for evaluating feeding behavior and body
condition.

Within groups, it is helpful to identify high-risk does, does with
special needs, and does with a previous history of problems for closer
observation during feeding and for detection of early signs of ketosis.

For example, does expecting triplets (or more) could receive a red
plastic collar at time of vaccination 1 month before the expected due
date. In herds where does are not broken into groups by due date,
additional collars could be used to indicate month due. So a doe due to
kid in March might wear a red and a white collar, and so on. This gives
the observer added information to assess does by direct visual exam, and
can serve as a reminder of overdue management events when no collars
have been applied ([Rowe 2014](#ref-prevent)).

*Increase feeder space or partition pens to allow high-risk does
unlimited access to fresh feed*

The increasing abdominal mass of does in late gestation increases the
feedbunk space needed to feed the same number of does. As the doe's
agility declines in later gestation, her ability to maintain feeder
access is compromised, so the does most in need of uninterrupted access
to the best or freshest feed are often the least able to compete for it.
Adding extra feeders or dividing pens can improve feed access for
high-risk does.

*Fully utilize management information*

Owners should make a calendar schedule to start lead-feeding and other
management events. Especially early in the kidding season, errors like
missed vaccinations, delayed start of lead-feeding, and unexpected
kiddings are more likely to occur because elements of annual routines
are forgotten or slip by unnoticed.

Scheduling events in advance can help to keep herd practices in step
with herd management policies.

Among these, it is important to record breeding dates, date of confirmed
conception (in order to predict kidding-due dates) where information is
easily accessed by those who are observing the does during late
gestation and at the time of kidding and especially to assess, if
necessary, induction of parturition in case of toxemia (to minimize
complications that may arise at term in high-risk herds). Elective
induction of parturition is not recommended unless there is a high level
of confidence in the accuracy of doe identification, records, and
confirmed gestation dates, as errors in any of these can result in the
unintended abortion of a normal pregnancy.

Proper application of management information can be an undisputable tool
that allows owners to make better decisions about intervention and
interpretation of changing behavior, the recognition of fluctuations in
the animal's body condition score and clinical status, possible dystocia
or retained kids at parturition, all of which are essential, along with
early clinical signs of the disease, for prompt intervention in case of
PT ([Rowe 2014](#ref-prevent)).

*Additional tools that owners can use for pregnancy toxemia*

Owners transporting does to the veterinary clinic should have a urine
cup ready during loading and unloading and collect a sample as
opportunity arises.

Owners should keep a stethoscope and thermometer in the barn to
facilitate complete examination of any doe with decreased appetite,
depression, abnormal respiratory pattern, diarrhea or possible signs of
normal or premature labor.

Early recognition and prompt treatment of pneumonia or other disease, as
well as pregnancy toxemia, is critical to maintaining doe health and
preventing death loss.

Keep plastic trash bags and plastic gloves in the barn to remove
products of abortion or to retrieve and dispose of placentas.

Necropsy of does lost during late gestation or after kidding is
important so that prevention or treatment strategies can be implemented
to address the primary and contributing causes of death and prevent
future losses. Vigilance in detecting abortion and submitting samples
for diagnosis of contagious and potentially zoonotic causes of abortion
are important for protecting the herd and the humans caring for them
([Rowe 2014](#ref-prevent)).

*Dry period management*

Owners are sometimes tempted to use the dry period to reduce fleshing in
overweight does. This should be avoided in late gestation. Poor
nutrition of otherwise healthy does during late gestation impacts birth
weight of kids, colostral quality, lactation performance, and risk of
postpartum metabolic disease.

A dairy doe should be dried-off 60 days before her expected due date.

Accurate prediction of the 60-day dry period is needed to avoid an
antibiotic residue when intramammary dry-treatment is used; proper
dry-treatment is necessary for adequate colostrum secretion and optimal
milk production in the ensuing lactation ([Rowe 2014](#ref-prevent)).

## Regulation of food and feed intake

Goats and sheep are known to be animals capable of exploiting a wide
range of plant varieties, including shrub, tree and herbaceous species;
they are capable of grazing very low grass and feeding on plants rich in
spines and thorns; in particular, goats are capable of standing on their
hindlimbs so as to pick up leaves and plant parts placed at considerable
heights.

Due to their physiological and anatomical characteristics, they exhibit
particularly flexible feeding behavior. In fact, they are defined as
intermediate feeders, that is, with characteristics that place them
between the large grazing ruminants, grass roughage eaters, and grazing
ruminants, concentrate selectors.

The main feeding systems that are used in sheep and goat farming are:

- Hay and supplementary feed: a traditional system in which, along with hay, food is given in addition to provide specific nutrients; the type of supplement to feed depends on the energy and protein requirements of the sheep/goat, availability, forage quality, cost and convenience. First, it is advisable to get silage and hay analysed to understand what level of supplementation will be needed;
- Hay, supplementary feed and raw materials: involves more food to be supplied but meets more of the feeding needs. Raw materials used include alfalfa, beet pulp or cereals such as corn and barley;
- Hay and raw materials: system based on hay and purchase of raw materials, whether forages, energy or protein concentrates.

The greatest period in which a discrepancy between nutritional intake
and nutritional requirement occurs is in the spring season, during the
last months of gestation and at the beginning of lactation
([Pulina 2005](#ref-caspita); [Decandia et al. 2005](#ref-caspitaa)).

The ultimate goal for this period is for ewes and does to consume a
ratio that will promote embryo survival, results in a healthy set of
twins, and ensures plenty of high quality colostrum and milk to consume
([Barkley 2019](#ref-introduzcibo)).

In the initial dry period, up to the third month of gestation, it is
sufficient to maintain an energy concentration of the ratio around 0.65
milk forage units (LFS)/kg dry matter (DM) by feeding only forages; to
be raised to 0.85 LFS/Kg of DM in the fifth and final stage of
pregnancy.

In order to achieve these levels of energy concentration, it is
essential not to overuse concentrated feed, but rather to use quality,
palatable forages in order to keep the rumen tonic and voluminous (which
is essential to avoid decreased ingestion at the beginning of lactation)
([Pulina 2005](#ref-caspita); [Decandia et al. 2005](#ref-caspitaa)).

During this period, the use of forages and/or concentrates with a high
crude protein (CP) value is crucial, especially in the case of
pasturing, if the percentage of herbaceous species is very restricted.
For a 70kg ewe carrying twins, the MP requirement between seven weeks
and one week pre-lambing increases by 60%; these requirements do vary
according to her weight, BCS, the number of lambs carried and whether
she is gaining, losing or maintaining weight ([James 2018](#ref-farm)).

In general, high energy supplements (such as grain-based concentrates)
can easily allow recovery of body reserves, but may result in reduced
grazing.

It is of outmost importance not to overfeed during this period if the
animals already have a good body condition because they would gain too
much weight (BCS greater than 3.75).

In order to stimulate the animal to eat more during this late gestation
period, it might also be preferable to feed grain instead of hay, thus
reducing the bulking effect that is one of the causes of reduced
ingestion ([Pulina 2005](#ref-caspita); [Decandia et al. 2005](#ref-caspitaa)). During the last 6 weeks of gestation,
grain is required as a source of carbohydrates in the ration to maintain
the health of multiple-bearing ewes. The amount varies depending on
forage quality, adult body weight, condition score and number of fetuses
([Kelay and Assefa 2018](#ref-PregnancyToxemiaEwe)).

The recommended daily ratio can be composed as follows:

- Top quality hay at will, hay intake will be approximately 1.5 to 2.5 kg/100 kg body weight;
- If available hourly grass and supplementary feed in larger quantities than in earlier gestation stages and gradually increasing as the delivery approaches (especially with multiple pregnancies). Without availability of grass, supplementary feed should be given in larger amounts than the previous situation so that it also ensures good vitamin and mineral supplementation with particular regard to calcium, phosphorus and magnesium;
- Check calcium/phosphorus balance if large amounts of brassicas or sugar beet pulp are being fed. 70 kg doe requires 6.0 g calcium and 4.2 g phosphorus/day. Excess in calcium intake increases the risk of milk fever;
- Potable and clean water at will.

Regarding the management of the entire pregnancy period, it can be
concluded that optimal dietary management of ewes throughout the
pregnancy period should result in improved body condition from the time
of mounting until parturition, safeguarding the welfare of the animals
([Cangemi et al. 2014](#ref-cibo)).

## Body condition score (BCS) monitoring

Body condition score is the best indicator of available fat reserves
which can be used by the animal in periods of high energy demand,
stress, or suboptimal nutrition. It is related both with intermuscular
fat in the lumbar region and the fat depth above the longissimus dorsi
muscle, along with the depth of the longissimus dorsi muscle itself
(which comprises both muscle and intramuscular fat). Body condition
scoring system (BCSS) is a useful tool for documenting the suitability
of the feeding program and should be monitored on a regular basis to
ensure that:

- Ewes and does are not overconditioned during early gestation;
- Nutritional planes rise during the second half of gestation.

Ewes and does are generally scored on a 1-through-5 system, with a
condition score of 1 being extremely thin and a score of 5 being obese.

![Body condition score in ewes and goats](/posts/img/pregnancy_toxemia/bcs2.jpg)

BCS cannot be assigned by simply looking at the cattle. Instead, the dam
must be touched and felt.

The first body area to feel in determining BCS is the lumbar area, which
is the back behind the ribs containing the loin. Scoring in this area is
based on determining the amount of muscle and fat over and around the
vertebrae. Lumbar vertebrae have a vertical protrusion (spinous process)
and two horizontal protrusions (transverse processes). Operators should
run their hand over this area and try to grasp these processes with
their fingertips and hand.

The second body area to feel is the fat covering on the sternum
(breastbone). Scoring in this area is based upon the amount of fat that
can be pinched.

A third area is the rib cage and fat cover on the ribs and intercostal
(between ribs) spaces ([Villaquiran et al. 2004](#ref-bcs)).

However, the distribution of the body fat in goats differs appreciably
from ewes. Fat deposits in dorsal region are in fact not highly
noticeable, and when the BCS method has used the dorsal and lumbar
regions to predict fat stores in goats, the results have not proven to
be very satisfactory. For this reason, the best indicator has been
determined to be the sternal region, where large amounts of fat
accumulate in goats ([Koyuncu and Altınçekiç 2012](#ref-bodycs)).

![Lumbar region examination](/posts/img/pregnancy_toxemia/lombare.png)

It is common for BCS to decline by about one-half score during the
second and third months of pregnancy; however, this condition should be
corrected by the time the dam enters the fourth month of pregnancy.
Variations in excess of one half of a body condition score during early
pregnancy can lead to overly thin or overly fat ewes during late
gestation.

Typically, recommended body condition scores fluctuate with the major
production periods of the animal. It is important that producers and
veterinarians regularly score a small percentage of the flock or herd to
determine average values; however, producers who view their flock on a
daily basis are often poor evaluators of body condition (gradual change
goes unnoticed).

It is often easier for an infrequent and impartial observer, such as a
local veterinarian, to detect gradual condition changes that might go
unnoticed by producers. Early detection of body condition excesses or
deficiencies is important.

In general, small ruminants are capable of rapid response to dietary
changes. Also, the health, production, and economic implications of
overfeeding and underfeeding are especially severe if either occurs in
critical phase of production.

Producers should be encouraged to adjust pasture stocking rates or feed
quality to maintain an average condition score of 2 to 2.5 during
maintenance. Ewes should gain weight during flushing, but condition
scores should not increase past the 3 to 3.5 level.

<table>
<tr>
<th colspan="2">Suggested BCS for ewes and does during various stages of production</th>
</tr>
<tr>
<th>Maintenance</th>
<td>2.0 - 2.5</td>
</tr>
<tr>
<th>Breeding</th>
<td>3.0</td>
</tr>
<tr>
<th>Early pregnancy (first trimester)</th>
<td>3.0</td>
</tr>
<tr>
<th>Midpregnancy (second trimester)</th>
<td>2.5 - 3.0</td>
</tr>
<tr>
<th>Late pregnancy (third trimester)</th>
<td>3.0 - 3.5</td>
</tr>
<tr>
<th>Lambing</th>
<td>3.5</td>
</tr>
<tr>
<th>Weaning</th>
<td>2.0 - 2.5</td>
</tr>
</table>

As a rule, during the last 6 weeks of pregnancy, mature ewes and does
pregnant with single fetuses should increase their body weight by about
10%. Correspondingly, ewes or does carrying twins should exhibit an 18%
increase in body weight. Average body condition flock/herd scores should
be maintained at or increased to a 3 to 3.5 score at parturition.

Practitioners faced with the task of preventing thin or fat ewe or doe
pregnancy toxemia at a flock or herd level need to develop two separate
feeding management strategies designed for each group of animals. Older
does (\>7 years) should also be watched carefully as their feed
requirements increase in order to maintain their body condition, and
higher energy density feeds must be assessed.

Thin late gestation ewes and does with minimal body scores (2.5 or
below) are at increased risk of clinical ketosis due insufficient energy
reserves to meet the late gestational mismatch between energy demands
and dry matter intake; they should be sorted (if possible) into separate
feeding groups and provided a more energy-dense diet than the
recommended ratio for ewes with normal BCS. The energy level and density
of the diet should also be increased to adjust for housing, weather
condition, and fleece/hair status.

If thin ewes or does are numerous and logistics prevent sorting, it is
usually wise to feed the entire production unit at the higher energy
level.

In contrast, obese dam (4.5 or greater) have increased risk of
compromise from hepatic lipidosis (fatty liver disease) as well as
reduced potential abdominal capacity (limiting feed intake) from
accumulation of omental fat. The body condition of the animal can often
be related to social factors within the animal group: often dominant
ewes/goats can steal almost all the food from the other goats and doing
so get overweight.

Among the various management strategies that can be pursued to reduce
animal weight, it is necessary to consider increasing walking time
through exercise where possible, and gradually reducing the proportion
of feed available.

Fat ewe pregnancy toxemia that results from overfeeding during early
pregnancy is a common problem for:

- Small farm flocks or herds;
- Beginning producers;
- Systems where high-energy forages are fed ad libitum during early pregnancy.

Pregnancy toxemia resulting from underfeeding and starvation is a common
problem with larger, grazing-oriented commercial flocks or herds.
Because of ewe or doe numbers, larger commercial flocks and herds are
severely impacted by drought induced feed shortages, adverse weather
conditions, and lack of housing facilities.

A more practical approach to using body condition information in
management of gestating does is to monitor the pen/group, look at the
overall status, and then identify does that stand out as below or above
the optimum. Decreased body condition in 1 or 2 individuals could be
addressed by increasing supplementation of those individuals; decreased
body condition in more than 1 or 2 individuals reflects a group problem
with energy balance and should be addressed by increasing
supplementation for the entire group in order to prevent loss of
condition in additional does. To utilize this strategy, the "group"
needs to be defined as animals in similar stage of gestation, either by
pen assignment or visible ID (such as color-coded collars)
([Rook 2000](#ref-VetClinNAmerica); [Rowe 2014](#ref-prevent)).

# Conclusion

Pregnancy toxemia is therefore displayed as a disease of remarkable
importance in the world of small ruminant farming, as the prognosis
inflicted by the disease is considerably negative, because both the poor
outcome of the clinically affected animals, even with proper treatment,
and of the impact caused by triggers such as hypofeeding and concomitant
diseases that can afflict animal morbidity, fetal survival, and flock
productivity. Cases of PT are an indication of the flaws in the
management of sheep an goats in late pregnancy.

This is one of the main reasons why it is necessary to know the disease
thoroughly and learn to recognize its signs in a timely manner. The role
of the farmer is in fact a key component for prevention in the field of
this disorder. Creating proper feeding habits, timely monitoring of the
animal's behavior and BCS prove to be the crucial factors in preventing
the onset of the disease.

<!-- Illustrations in figure [4](#NEB){reference-type="ref" reference="NEB"},
by FarmHealth online.

Illustrations in figures [\[KBS\]](#KBS){reference-type="ref"
reference="KBS"}, [\[KB\]](#KB){reference-type="ref" reference="KB"},
[2](#stargazing_goat){reference-type="ref" reference="stargazing_goat"},
[3](#Ataxia and in-coordination){reference-type="ref"
reference="Ataxia and in-coordination"},
[5](#processi){reference-type="ref" reference="processi"} by Alessandra
Gallia.
-->

---

# Bibliography

<ul>
    <li id="ref-DiseasesOfSheep" class="csl-entry" role="doc-biblioentry">
    Aikten, I. D., ed. 2007. <em>Diseases of Sheep</em>. Vol. 4. Blackwell.
    </li>
    <li id="ref-pipi" class="csl-entry" role="doc-biblioentry">
    Anderson, David E., and D. Michael Rings, eds. 2009. <em>Current
    Veterinary Therapy: Food Animal Practice</em>. Vol. 5. Saunders
    Eslevier.
    </li>
    <li id="ref-rispimmun" class="csl-entry" role="doc-biblioentry">
    B. Ronchi, U. Bernabucci, N. Lacetera. 2008. <span>“Dismetabolie e
    Risposta Immunitaria Nei Piccoli Ruminanti: Il Caso Della Tossiemia
    Gravidica.”</span> In, edited by Annarita Di Cerbo, Mario Luini, Maria
    Teresa Manfredi, and Marco Ruggeri, 4:138–40. Società Italiana
    Veterinari per Animali da Reddito.
    </li>
    <li id="ref-BovineKetosis" class="csl-entry" role="doc-biblioentry">
    Baird, G. David. 1982. <span>“Primary Ketosis in the High-Producing
    Dairy Cow: Clinical and Subclinical Disorders, Treatment, Prevention,
    and Outlook.”</span> <em>Journal of Dairy Science</em> 65 (1): 1–10.
    </li>
    <li id="ref-complications" class="csl-entry" role="doc-biblioentry">
    Barbagianni, M. S., S. A. Spanos, K. S. Ioannidi, N. G. C. Vasileiou, A.
    I. Katsafadou, I. Valasi, P. G. Gouletsou, and G. C. Fthenakis. 2015.
    <span>“Increased Incidence of Peri-Parturient Problems in Ewes with
    Pregnancy Toxaemia.”</span> <em>Small Ruminant Research</em> 132:
    111–14. https://doi.org/<a
    href="https://doi.org/10.1016/j.smallrumres.2015.10.017.">https://doi.org/10.1016/j.smallrumres.2015.10.017.</a>
    </li>
    <li id="ref-introduzcibo" class="csl-entry" role="doc-biblioentry">
    Barkley, Melanie. 2019. <span>“Monitor Body Condition Scoring and
    Nutrition Throughout Pregnancy.”</span> <em>PennState Extension</em>,
    1–4. <a
    href="https://extension.psu.edu/monitor-body-condition-scoring-and-nutrition-throughout-pregnancy">https://extension.psu.edu/monitor-body-condition-scoring-and-nutrition-throughout-pregnancy</a>.
    </li>
    <li id="ref-PregnancyMelanie" class="csl-entry" role="doc-biblioentry">
    Boileau, Melanie J. 2008. <span>“Pregnancy Toxemia in Small
    Ruminants.”</span> VIN. 2008. <a
    href="https://www.vin.com/apputil/content/defaultadv1.aspx?pId=11262&amp;id=3865531&amp;print=1">https://www.vin.com/apputil/content/defaultadv1.aspx?pId=11262&amp;id=3865531&amp;print=1</a>.
    </li>
    <li id="ref-identif" class="csl-entry" role="doc-biblioentry">
    Bortolotti, Laura. 2019. <span>“Sistema Di Identificazione e
    Registrazione Degli Animali (IR).”</span> <em>Istituto Zooprofilattico
    Sperimentale Delle Venezie</em>, 1–71. <a
    href="https://www.venetoagricoltura.org/wp-content/uploads/2019/11/Bortolotti_.pdf">https://www.venetoagricoltura.org/wp-content/uploads/2019/11/Bortolotti_.pdf</a>.
    </li>
    <li id="ref-diagnosis" class="csl-entry" role="doc-biblioentry">
    Brahma, Gowri B., Chandrasekaran D., and Arunaman C. S. 2019.
    <span>“Successful Medical Management of Pregnancy Toxemia in
    Goats.”</span> <em>Journal of Animal Research</em>, 837–42. <a
    href="https://doi.org/DOI: 10.30954/2277-940X.06.2019.6">https://doi.org/DOI:
    10.30954/2277-940X.06.2019.6</a>.
    </li>
    <li id="ref-cibo" class="csl-entry" role="doc-biblioentry">
    Cangemi, Mario, Carta Graziano, Fadda Maria Giovanni, and Fraghì Pietro.
    2014. <em>Note Tecniche Sull’alimentazione Degli Ovini e Dei
    Caprini</em>. 3rd ed. Laore Agenzia regionale per lo svilippo in
    agricoltura.
    </li>
    <li id="ref-caspitaa" class="csl-entry" role="doc-biblioentry">
    Decandia, Mauro, Giovanni Molle, Giuliano Pinna, Andrea Cabiddu, and
    Maria Yiakoulaki. 2005. <span>“Comportamento Alimentare Ed Ingestione Di
    Capre Al Pascolo Su Macchia Mediterranea.”</span> In, edited by Pulina
    G., 173–94. Avenue media.
    </li>
    <li id="ref-VeterinaryMedicine" class="csl-entry"
    role="doc-biblioentry">
    Done, S. H., D. E. Jacobs, B. O. Ikede, and R. A. McKenzie et al. 2006.
    <span>“Pregnancy Toxemia in Sheep.”</span> In, edited by Otto M.
    Radostis and Clive C. Gay et al., 1668–71.
    </li>
    <li id="ref-IncidenceGoats" class="csl-entry" role="doc-biblioentry">
    Dubuc, V. Dorè J., A. M. Bèlanger, and S. Buczinski. 2015.
    <span>“Definition of Prepartum Hyperketonemia in Dairy Goats.”</span>
    <em>Journal of Dairy Science</em> 98 (7): 4535–43.
    </li>
    <li id="ref-YearbookofAgriculture" class="csl-entry"
    role="doc-biblioentry">
    Dye, Joseph A., and Robert W. Dougherty. 1956. <span>“Ketosis in
    Cattle.”</span> In, edited by Alfred Steffeurd, 250–55. Animal Diseases.
    The Yearbook of Agriculture.
    </li>
    <li id="ref-SymposiumonSheepandGoatMedicine" class="csl-entry"
    role="doc-biblioentry">
    East, Nancy E. 1983. <span>“Pregnancy Toxemia, Abortions, and
    Periparturient Diseases.”</span> Edited by Mary C. Smith. <em>Symposium
    on Sheep and Goat Medicine</em> 5 (3): 601–18.
    </li>
    <li id="ref-FoodAnimalPractice" class="csl-entry"
    role="doc-biblioentry">
    Edmondson, Misty A., and David G. Pugh. 2009. <span>“Pregnancy Toxemia
    in Sheep and Goats.”</span> In, edited by David E. Anderson and D.
    Michael Rings, 5:144–45.
    </li>
    <li id="ref-Differentials" class="csl-entry" role="doc-biblioentry">
    Erickson, Anna. 2020. <span>“Pregnancy Toxaemia and Hypocalcaemia of
    Ewes.”</span> Agriculture and Food. 2020. <a
    href="https://www.agric.wa.gov.au/livestock-biosecurity/pregnancy-toxaemia-and-hypocalcaemia-ewes ">https://www.agric.wa.gov.au/livestock-biosecurity/pregnancy-toxaemia-and-hypocalcaemia-ewes
    </a>.
    </li>
    <li id="ref-Ruminantia" class="csl-entry" role="doc-biblioentry">
    Fantini, Alessandro. 2011. <span>“La Chetosi Della Vacca Da
    Latte.”</span> <em>Ruminantia</em>, 146–51. <a
    href="https://www.ruminantia.it/wp-content/uploads/2016/05/LA-CHETOSI-NELLA-VACCA-DA-LATTE.pdf">https://www.ruminantia.it/wp-content/uploads/2016/05/LA-CHETOSI-NELLA-VACCA-DA-LATTE.pdf</a>.
    </li>
    <li id="ref-spieg" class="csl-entry" role="doc-biblioentry">
    ———. 2019. <span>“La Tossiemia Gravidica Della Pecora e Della
    Capra.”</span> Ruminantia. Ruminantia. 2019. <a
    href="https://archivio.ruminantia.it/la-tossiemia-gravidica-della-pecora-e-della-capra/">https://archivio.ruminantia.it/la-tossiemia-gravidica-della-pecora-e-della-capra/</a>.
    </li>
    <li id="ref-FarmHealthOnlineToxemia" class="csl-entry"
    role="doc-biblioentry">
    Farm Health Online. 2018. <span>“Pregnancy Toxemia and Ketosis in
    Goats.”</span> Farm Health Online. 2018. <a
    href="https://www.farmhealthonline.com/US/disease-management/goat-diseases/pregnancy-toxaemia-in-goats/">https://www.farmhealthonline.com/US/disease-management/goat-diseases/pregnancy-toxaemia-in-goats/</a>.
    </li>
    <li id="ref-polioenc" class="csl-entry" role="doc-biblioentry">
    Fecteau, Gilles, and Lisle W. George. 2009. <span>“Mentation
    Abnormality, Depression, and Cortical Blindness.”</span> In, edited by
    Elsevier, 5:307–8. Penny Rudolph.
    </li>
    <li id="ref-treatment" class="csl-entry" role="doc-biblioentry">
    George, Fthenakis. 2022. <span>“Pregnancy Toxemia in Sheep and
    Goats.”</span> MSD Manual Veterinary Manual. 2022. <a
    href="https://www.msdvetmanual.com/metabolic-disorders/hepatic-lipidosis/pregnancy-toxemia-in-sheep-and-goats">https://www.msdvetmanual.com/metabolic-disorders/hepatic-lipidosis/pregnancy-toxemia-in-sheep-and-goats</a>.
    </li>
    <li id="ref-rotera" class="csl-entry" role="doc-biblioentry">
    Giri, Dhurba. 2018. <span>“Rothera’s Test for Ketone Bodies: Principle,
    Procedure and Clinical Significances.”</span> Laboratory Tests. 2018. <a
    href="https://laboratorytests.org/rotheras-test/">https://laboratorytests.org/rotheras-test/</a>.
    </li>
    <li id="ref-KetoneBodies" class="csl-entry" role="doc-biblioentry">
    Gulinski, Piotr. 2021. <span>“Ketone Bodies – Causes and Effects of
    Their Increased Presence in Cows’ Body Fluids: A Review.”</span>
    <em>Veterinary World</em> 14 (6): 1492–1503.
    </li>
    <li id="ref-MeeraHeller" class="csl-entry" role="doc-biblioentry">
    Heller, Meera. n.d. <span>“Pregnancy Toxemia in Sheep and Goats.”</span>
    <a
    href="https://animalscience.ucdavis.edu/sites/g/files/dgvnsk446/files/inline-files/2017-dr-meera-heller-prognancy-toxemia.pdf">https://animalscience.ucdavis.edu/sites/g/files/dgvnsk446/files/inline-files/2017-dr-meera-heller-prognancy-toxemia.pdf</a>.
    </li>
    <li id="ref-farm" class="csl-entry" role="doc-biblioentry">
    James, Debbie. 2018. <span>“Advice on Feeding Pregnant Ewes When Winter
    Fodder Stocks Low.”</span> Farmers Weekly. 2018. <a
    href="https://www.fwi.co.uk/livestock/livestock-feed-nutrition/advice-on-feeding-pregnant-ewes-when-winter-fodder-stocks-low">https://www.fwi.co.uk/livestock/livestock-feed-nutrition/advice-on-feeding-pregnant-ewes-when-winter-fodder-stocks-low</a>.
    </li>
    <li id="ref-ketoneb" class="csl-entry" role="doc-biblioentry">
    Jensen, N. J., H. Z. Wodschow, M. M. Nilsson, and J. Rungby. 2020.
    <span>“Effects of Ketone Bodies on Brain Metabolism and Function in
    Neurodegenerative Diseases.”</span> <em>International Journal of
    Molecular Sciences</em>. <a
    href="https://doi.org/10.3390/ijms21228767">https://doi.org/10.3390/ijms21228767</a>.
    </li>
    <li id="ref-previ" class="csl-entry" role="doc-biblioentry">
    Jesse, Faez Firdaus Abdullah, Eric Lim Teik Chung, Wan Nurhakimah Wan
    Zakaria, and Yusuf Abba. 2016. <span>“Clinical Case of Pregnancy
    Toxaemia in a Goat, a Case Report on Veterinary Medicine
    Approach.”</span> <em>International Journal of Livestock Research</em>
    6: 76–81. https://doi.org/<a
    href="http://dx.doi.org/10.5455/ijlr.20160613094613">http://dx.doi.org/10.5455/ijlr.20160613094613</a>.
    </li>
    <li id="ref-PregnancyToxemiaEwe" class="csl-entry"
    role="doc-biblioentry">
    Kelay, Ashenafi, and Aschalew Assefa. 2018. <span>“Causes, Control and
    Prevention Methods of Pregnancy Toxemia in Ewe: A Review.”</span>
    <em>Journal of Life Science and Biomedicine</em> 8 (4): 69–76.
    </li>
    <li id="ref-bodycs" class="csl-entry" role="doc-biblioentry">
    Koyuncu, Mehmet, and Şeniz Öziş Altınçekiç. 2012. <span>“Importance of
    Body Condition Score in Dairy Goats.”</span> <em>Macedonian Journal of
    Animal Science</em> 3 (2): 167–73.
    </li>
    <li id="ref-esercizio" class="csl-entry" role="doc-biblioentry">
    Krebs, Rebecca. 2022. <span>“The Importance of Excercise for Pregnant
    Does.”</span> <em>Goat Journal</em> 100 (1): 20–23.
    </li>
    <li id="ref-KetosisMilkFeverGrass" class="csl-entry"
    role="doc-biblioentry">
    Littledike, E. T., J. W. Young, and D. C. Beitz. 1981. <span>“Common
    Metabolic Diseases of Cattle: Ketosis, Milk Fever, Grass Tetany, and
    Downer Cow Complex.”</span> <em>Journal of Dairy Science</em> 64 (6):
    1465–82.
    </li>
    <li id="ref-KetosisinDairyCattleFarms" class="csl-entry"
    role="doc-biblioentry">
    Madreseh-Ghahfarokhi, Samin, Azam Dehghani-Samani, and Amir
    Dehghani-Samani. 2018. <span>“Ketosis (Acetonaemia) in Dairy Cattle
    Farms: Practical Guide Based on Importance, Diagnosis, Prevention and
    Treatments.”</span> <em>MedCrave, Journal of Dairy, Veterinary &amp;
    Animal Research</em> 7 (6): 299‒302.
    </li>
    <li id="ref-PTJudith" class="csl-entry" role="doc-biblioentry">
    Marteniuk, Judith V., and Thomas H. Herdt. 1988. <span>“Pregnancy
    Toxemia and Ketosis of Ewes and Does.”</span> <em>Veterinary Clinics of
    North America: Food Animal Practice</em> 4 (2): 307–15.
    </li>
    <li id="ref-diseasesofthegoat" class="csl-entry"
    role="doc-biblioentry">
    Matthews, John. 2016. <span>“Diseases of the Goat.”</span> In, edited by
    4, 44–50. Blackwell.
    </li>
    <li id="ref-SindromeChetosica" class="csl-entry"
    role="doc-biblioentry">
    Pauluzzi, Luigi, and Gianfranco Valent. 1991. <em>La Sindrome Chetosica
    Del Bovino</em>. Brescia: Fondazione iniziative zooprofilattiche e
    zootecniche.
    </li>
    <li id="ref-DocentiuniNa" class="csl-entry" role="doc-biblioentry">
    <span>“Peri-Partum e Chetogenesi.”</span> n.d. <a
    href="https://www.docenti.unina.it/webdocenti-be/allegati/materiale-didattico/34024293">https://www.docenti.unina.it/webdocenti-be/allegati/materiale-didattico/34024293</a>.
    </li>
    <li id="ref-caspita" class="csl-entry" role="doc-biblioentry">
    Pulina, Giuseppe. 2005. <em>L’alimentazione Della Capra Da Latte</em>.
    Edited by Daniela Brandano. Avenue Media.
    </li>
    <li id="ref-ciao" class="csl-entry" role="doc-biblioentry">
    Radostis, Otto M., and Clive C. Gay et al., eds. 2006. <em>Veterinary
    Medicine. A Textbook of the Diseases of Cattle, Horses, Sheep, Pigs and
    Goats</em>. 10th ed. Saunders Elsevier.
    </li>
    <li id="ref-caspitaaa" class="csl-entry" role="doc-biblioentry">
    Raspetti, Luca, and Luciana Bava. 2005. <span>“Tecnologie Di
    Alimentazione Della Capra in Stalla.”</span> In, edited by Pulina G.,
    251–55. Avenue media.
    </li>
    <li id="ref-VetClinNAmerica" class="csl-entry" role="doc-biblioentry">
    Rook, Joseph S. 2000. <span>“Pregnancy Toxemia of Ewes, Does, and Beef
    Cows, from Veterinary Clinics of North America: Food Animal
    Practice.”</span> In, edited by Elsevier, 16:293–317. 2.
    </li>
    <li id="ref-prevent" class="csl-entry" role="doc-biblioentry">
    Rowe, Joan Dean. 2014. <span>“Teaching Goat Clients to Prevent Pregnancy
    Toxemia.”</span> <em>American Association of Bovine Practitioners</em>,
    no. 47 Animal conference: 99–103. https://doi.org/<a
    href="https://doi.org/10.21423/aabppro20143687">https://doi.org/10.21423/aabppro20143687</a>.
    </li>
    <li id="ref-Pathophisiology" class="csl-entry" role="doc-biblioentry">
    Schulz, Leanne M., and Richard L. Riese. 1983. <span>“Pregnancy Toxemia
    in the Ewe.”</span> <em>Iowa State University Veterinarian</em> 45 (1):
    11–15.
    </li>
    <li id="ref-AdvancesinAnimalHealth" class="csl-entry"
    role="doc-biblioentry">
    Simões, P. B. A., R. Bexiga, L. P. Lamas, and M. S. Lima. 2020.
    <span>“Pregnancy Toxaemia in Small Ruminants.”</span> In, edited by Luis
    Lopes da Costa and Antonio Freitas Duarte, 541–56.
    </li>
    <li id="ref-moro" class="csl-entry" role="doc-biblioentry">
    Smith, Mary C., and David M. Sherman. 2007. <em>Goat Medicine</em>.
    Edited by Blackwell.
    </li>
    <li id="ref-magnes" class="csl-entry" role="doc-biblioentry">
    Solaiman, Sandra G. 2010. <span>“Goat Science and Production.”</span>
    In, 170–71. Blackwell.
    </li>
    <li id="ref-popo" class="csl-entry" role="doc-biblioentry">
    Steffeurd, Alfred, ed. 1956. <em>Animal Diseases. The Yearbook of
    Agriculture</em>. USDA.
    </li>
    <li id="ref-alim" class="csl-entry" role="doc-biblioentry">
    Toteda, F. 2016. <span>“LEZIONI DI PRINCIPI DI NUTRIZIONE e
    ALIMENTAZIONE ANIMALE.”</span>
    </li>
    <li id="ref-bcs" class="csl-entry" role="doc-biblioentry">
    Villaquiran, M., T. A. Gipson, R. C. Merkel, A. L. Goetsch, and T.
    Sahlu. 2004. <span>“Body Condition Scores in Goats.”</span> American
    Institute for Goat Research. 2004. <a
    href="https://www.luresext.edu/sites/default/files/BCS_factsheet.pdf">www.luresext.edu/sites/default/files/BCS_factsheet.pdf</a>.
    </li>
</ul>
