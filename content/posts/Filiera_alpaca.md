---
title: "Filiera degli Alpaca"
description: "Analisi a tutto tondo sull'allevamento degli Alpaca"
date: 2022-01-28T01:25:33+01:00
tags:
  - alpaca
  - allevamento
  - legislazione
  - import
  - export
  - lana
  - fibra
  - latte
  - carne
  - yogurt
  - marketing
image: "/posts/img/Allevamento_alpaca_cover.jpg"
---

Ho affrontato con l'università, studiando economia delle produzioni zootecniche e agroalimentari, lo studio di filiere minori presenti in Italia e nel mondo.

Il progetto che ho deciso di sviluppare in gruppo insieme ai miei colleghi universitari, riguarda la filiera degli Alpaca.

Abbiamo affrontato una analisi a tutto tondo: dalla storia e le origini di questi camelidi, fino all'analisi dell'allevamento, attingendo ad informazioni tra le quali il tipo di allevamento che è possibile avviare, le spese economiche da affrontare, la cura e l'handling, la legislazione da osservare, e il tipo di retribuzione che è possibile ottenere da questo tipo di allevamento, con lo scopo di poter fornire una piccola guida e un ulteriore acknowledgement su questa realtà da un grande potenziale.

[{{< figure src="/download_icon.svg" class="texticon" >}}Clicca per scaricare il PDF della presentazione](/Presentazione_alpaca.pdf)
