---
title: "L'allevamento degli alpaca"
description: "Un approfondimento dalle origini all'allevamento, la legislazione e consigli pratici per neofiti"
date: 2021-12-12T23:19:06+01:00
tags:
  - alpaca
  - allevamento
  - legislazione
  - import
  - export
  - lana
  - fibra
  - latte
  - carne
  - yogurt
  - marketing
image: "/posts/img/Allevamento_alpaca_cover.jpg"
toc: true
draft: true
---

## Introduzione

La ricerca che ho deciso di affrontare riguarda lo studio a tutto tondo del mondo degli alpaca, dalle origini storiche, all'allevamento, la cura e la normativa a riguardo, nella speranza che questo sia soltanto un punto di partenza nella mia crescita professionale e personale, e che un giorno possa essere ulteriormente aggiornato e approfondito.

È abbastanza desueto per la tradizione italiana parlare di questi tipi di filiere, in quanto, quando si parla di animali d'allevamento, i veri protagonisti sono i bovini, i suini, gli ovicaprini e il pollame; tuttavia occorre mettersi in discussione ed esplorare nuove realtà da un ottimo potenziale, e riflettere su quanto un allevamento non sia soltanto un sinonimo di sfruttamento animale, ma imparare ad osservarlo da un ottica di opportunità per entrare in armonia e studiare animali provenienti da storie e tradizioni lontane nel tempo e nello spazio. 

L'augurio che dedico ai futuri lettori, appassionati e curiosi, allevatori e amanti degli animali, è una piacevole lettura in questo percorso di analisi con interesse crescente, che possano sorgere domande e dibattiti a riguardo, perchè niente è più banale di un ambiente nel quale si è tutti d'accordo.

## Cenni storici



## Razze, genetica

## Territorio e allevamento

## Cura

## Situazione mondiale, import/ export, costi di produzione

## Produzione europea

## Caratteristiche morfologiche, organolettiche fibra

## Carne

## Latte

## Legislazione e certificazioni
